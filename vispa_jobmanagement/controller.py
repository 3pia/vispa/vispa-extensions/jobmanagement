# -*- coding: utf-8 -*-

# imports
import cherrypy
import vispa.workspace
import logging
import os
import json
import rpyc

from vispa.controller import AbstractController
from vispa import AjaxException

logger = logging.getLogger(__name__)


class JobmanagementController(AbstractController):
    def __init__(self, extension):
        AbstractController.__init__(self)
        setattr(self, 'extension', extension)

        self.__load_job_list_result = False

        # default paths
        self.__batchManagerOutputFolder = "$HOME/.vispa/BatchSystem"

        self.__defaultmanager = "LocalBatchManager"
        readout = vispa.config('BatchSystem', 'manager', "")
        if readout in ["local", "LocalBatchManager"]:
            self.__defaultmanager = "LocalBatchManager"
            logger.debug("setting default manager to %s" % self.__defaultmanager)
        elif readout in ["grid", "GridBatchManager"]:
            self.__defaultmanager = "GridBatchManager"
        elif readout in ["lsf", "LSFBatchManager"]:
            self.__defaultmanager = "LSFBatchManager"
        elif readout in ["hpc", "LSFBatchManager"]:
            self.__defaultmanager = "LSFBatchManager"
        elif readout in ["condor", "CondorBatchManager"]:
            self.__defaultmanager = "CondorBatchManager"
        else:
            logger.info(
                "Could not recognize the shortcut of the default manager in vispa.ini configuration. Use one out of "
                "'%s' or use the exact name, e.g. 'CondorBatchManager' (without quotation marks)." % (
                    "'local','grid','lsf','hpc','condor'"))
        logger.info("Default batchsystem is '%s' ." % self.__defaultmanager)

    def _manager(self, manager=None):
        if not manager:
            manager = self.__defaultmanager
            logger.debug("_manager: setting default manager to %s" % manager)
        try:
            m = vispa.workspace.get_instance("batchsystemmanager." + manager,
                                             init_args=self.__batchManagerOutputFolder,
                                             key=manager)
            return m
        except Exception, e:
            vispa.log_exception()
            raise Exception("Couldn't get rpc instance of \
                 batchsystemmanager! <br />Reason: %s" % str(e))

    def _get_manager_view(self, bm):
        try:
            return vispa.workspace.get_instance(
                "batchsystemmanager.abstract.AbstractBatchJobView",
                init_args={'bm': bm}, key=bm)
        except Exception, e:
            vispa.log_exception()
            raise Exception("Couldn't get rpc module \
                 AbstractBatchJobView! <br />Reason: %s" % str(e))

    @cherrypy.expose
    def get_available_batchmanager(self):
        bsm = vispa.workspace.module("batchsystemmanager")
        descriptions = bsm.getDescriptions()
        reply= [{"name": key, "description": value} for key, value in descriptions.iteritems()]
        return reply

    @cherrypy.expose
    def get_batchmanager_configuration(self, batchmanager):
        extid = cherrypy.request.private_params["_viewId"]
        bm = self._manager(batchmanager)
        if batchmanager == "CondorBatchManager":
            bm.updateCondorStatus()
            userpriorities = bm.getUserPriorities()[1:]
            userpriority_dict = []
            for user in userpriorities:
                userpriority_dict.append({
                    "userName": user[0],
                    "effectivePriority": user[1],
                    "priorityFactor": user[2],
                    "resInUse": user[3],
                    "totalUsage": user[4],
                    "timeSinceLastUsage": user[5]})
            content = {"availableCores": str(bm.getAvailableCores()),
                       "coresClaimedByOwner": str(bm.getCoresClaimedByOwner()),
                       "coresClaimedByCondor": str(bm.getCoresClaimedByCondor()),
                       "unclaimedCores": str(bm.getUnclaimedCores())}
            content_dict = {"extid": extid, "batchmanager": batchmanager,
                            "userpriorities": userpriority_dict,
                            "availableCores": content["availableCores"],
                            "coresClaimedByOwner": content["coresClaimedByOwner"],
                            "coresClaimedByCondor": content["coresClaimedByCondor"],
                            "unclaimedCores": content["unclaimedCores"]}
        elif batchmanager == "LocalBatchManager":
            content_dict = {"extid": extid, "batchmanager": batchmanager,
                            "maxRunning": bm.getMaxRunning()}
        elif batchmanager == "LSFBatchManager":
            content_dict = {"extid": extid, "batchmanager": batchmanager}
        elif batchmanager == "GridBatchManager":
            content_dict = {"extid": extid, "batchmanager": batchmanager}
        else:
            content_dict = {"extid": extid, "batchmanager": batchmanager}

        return content_dict

    def submit(self, command, manager=None, pre_execution_script_text="",
               post_execution_script_text="", notification='Never', queue=1,requirements=None,memory=2048,cpus=1,gpus=0):
        bm = self._manager(manager)
        _pre_execution_script_text = pre_execution_script_text
        _post_execution_script_text = post_execution_script_text

        if _pre_execution_script_text is None:
            _pre_execution_script_text = ""

        if _post_execution_script_text is None:
            _post_execution_script_text = ""

        _jobOptions = None
        if requirements!= None and requirements!="":
            _jobOptions=['requirements = {}'.format(requirements)]
        if manager == "CondorBatchManager":
            jobid = bm.addJob(command, _pre_execution_script_text,
                              _post_execution_script_text, _jobOptions,
                              notification=notification, queue=queue,memory=memory,cpus=cpus,gpus=gpus)
        else:
            jobid = bm.addJob(command, _pre_execution_script_text,
                             _post_execution_script_text, _jobOptions)
        logger.info("Job '%s' successfully submitted to batch manager" % jobid)
        return 0

    @cherrypy.expose
    @cherrypy.tools.json_parameters()
    def submit_jobs(self, commands, manager, pre_execution_script="",
                    post_execution_script="", notification="Never", queue=1,requirements=None,memory=2048,cpus=1,gpus=0):
        queue = int(queue)
        if len(commands) == 0:
            logger.info("command list is empty")
            return 0
        for command in commands:
            if len(command) == 0:
                continue
            logger.debug("Executing job with command='%s', manager='%s', \
            preExecutionScriptText='%s', postExecutionScriptText='%s' " % (command, manager, pre_execution_script,
                                                                           post_execution_script))

            self.submit(command, manager, pre_execution_script,
                        post_execution_script, notification, queue=queue,requirements=requirements,memory=memory,cpus=cpus,gpus=gpus)

    def restart_job(self, jobid, manager=None):
        self._manager(manager).restartJob(jobid)

    @cherrypy.expose
    @cherrypy.tools.json_parameters()
    def restart_jobs(self, jobids, manager=None):
        self._manager(manager).restartJobs(jobids)

    def remove_job(self, jobid, manager=None):
        logger.debug("removing job %s" % jobid)
        logger.debug("manager is %s" % manager)
        result = self._manager(manager).removeJob(jobid)
        if result is False:
            logger.debug("job %s could not be removed" % jobid)

    @cherrypy.expose
    @cherrypy.tools.json_parameters()
    def is_async_ready(self, manager=None):
        bm = self._manager(manager)
        if bm.isAsyncReady():
            logger.debug("removing jobs ready, resetting async")
            bm.resetAsyncStatus()
            return {'success': True}
        elif bm.isAsyncRunning():
            logger.debug("async is still running")
            return {'success': False, 'info': bm.getAsyncProgress()}
        else:
            logger.debug("no async job running")
            return {'success': True, 'info': "no async job was running."}

    @cherrypy.expose
    @cherrypy.tools.json_parameters()
    def remove_jobs(self, jobids=[], manager=None, async=False):
        if async:
            logger.debug("removing jobs async")
            bm = self._manager(manager)
            if bm.isAsyncReady():
                logger.debug("removing jobs ready, resetting async")
                bm.resetAsyncStatus()
                return {'success': True}
            elif bm.isAsyncRunning():
                logger.debug("removing jobs is still running")
                return {'success': False, 'info': bm.getAsyncProgress()}
            else:
                logger.debug("removing jobs not running yet, starting async request")
                bm.removeJobs(jobids, async=True)
                return {'success': False, 'info': "starting async"}
        else:
            self._manager(manager).removeJobs(jobids)

    def stop_job(self, jobid, manager=None):
        self._manager(manager).stopJob(jobid)

    @cherrypy.expose
    @cherrypy.tools.json_parameters()
    def stop_jobs(self, jobids, manager=None):
        try:
            logger.debug("stopping jobs " + str(jobids))
            self._manager(manager).stopJobs(jobids)
#             for jobid in jobids:
#                 self.stop_job(jobid, manager)
        except Exception, e:
            raise AjaxException(str(e))

            # @cherrypy.expose
            # @cherrypy.tools.ajax()
            # def get_job_details(self, jobid, manager=None):
            # stdout = self.getJobOutput(jobid, manager)
            # stderr = self.getJobErrorOutput(jobid, manager)
            # return {"stdout": stdout, "stderr": stderr}

    @cherrypy.expose
    def get_job_output(self, jobid, manager=None):
        output = self._manager(manager).getJobOutputData(jobid)
        output_name = self._manager(manager).getJobOutputFileName(jobid)
        return {"content": output, "name": output_name}

    @cherrypy.expose
    def get_job_error_output(self, jobid, manager=None):
        attr = self._manager(manager).getJobErrorData(jobid)
        error_output_name = self._manager(manager).getJobErrorFileName(jobid)
        return {"content": attr, "name": error_output_name}

    @cherrypy.expose
    def get_job_log_output(self, jobid, manager=None):
        attr = getattr(self._manager(manager), "readJobLogFile", None)
        if callable(attr):
            log_output_name = self._manager(manager).getJobLogOutputFileName(jobid)
            return {"content": attr(jobid), "name": log_output_name}
        else:
            return {"content": "Batchjobmanager %s does not create logfiles." % manager,
                    "name": "none"}

            # @cherrypy.expose
            # @cherrypy.tools.ajax()
            # def getJobCommand(self, jobid, manager=None):
            # command = self._manager(manager).getJobCommand(jobid)
            # return command

            # @cherrypy.expose
            # @cherrypy.tools.ajax()
            # def get_job_status(self, jobid, manager=None):
            # status = self._manager(manager).get_job_status(jobid)
            # return status

    @cherrypy.expose
    @cherrypy.tools.ajax(encoded=True)
    @cherrypy.tools.json_parameters()
    def get_job_data(self, batchmanager=None, only_modified=False,
                     reverse=True, async=False, **kwargs):
        """returns information about all current jobs as list of
        dictionaries."""
        if not batchmanager:
            batchmanager = self.__defaultmanager
        logger.debug("getting jobdata for manager %s" % batchmanager)
        bm = self._manager(batchmanager)
        view = self._get_manager_view(bm)

        if async:
            if bm.isJobListLoading() or bm.isJobListUpdating():
                n_jobs_loaded = bm.getNumberOfWatchedJobs()
                n_tot = bm.getNumberOfJobsInDataPath()
                info = ""
                if(bm.isJobListLoading()):
                    info += "jobs loading... (%i/%i)" % (n_jobs_loaded, n_tot)
                if(bm.isJobListUpdating()):
                    info += "jobs loaded (%i/%i)" % (n_jobs_loaded, n_tot)
                    info += "\nupdating jobs..."
                return json.dumps({'success': False, 'data': {}, 'info': info})
            else:
                from time import time

                if (time() - bm.getTimeOfLastUpdate()) < 10:  # if job list is up to date return joblist
                    logger.debug("job list up to date, returning job list")
                    kwargs.update({'encode_json': True})
                    kwargs.update({'reverse': reverse})
                    if only_modified:
                        data = view.getModifiedJobs(**kwargs)
                    else:
                        data = view.getJobs(**kwargs)
                    return '%s,"data":%s}' % (json.dumps({"success": True})[:-1], data)
                else:
                    logger.debug("job list not up to date, loading/updating job list")
                    bm.loadJobList(async=True)
                    return json.dumps({'success': False, 'data': {}, 'info': "start loading job list"})
        else:
            bm.loadJobList()
            kwargs.update({'encode_json': True})
            kwargs.update({'reverse': reverse})
            if only_modified:
                data = view.getModifiedJobs(**kwargs)
            else:
                data = view.getJobs(**kwargs)
            return '%s,"data":%s}' % (json.dumps({"success": True})[:-1], data)

            # logger.debug(kwargs)
            # if not batchmanager:
            # batchmanager = self.__defaultmanager
            # logger.debug("getting jobdata for manager %s" % batchmanager)
            #
            # if self.__load_job_list_result is not False:
            # if not self.__load_job_list_result.ready:
            # logger.debug("get_job_data(): still loading")
            # return json.dumps({'success': False, 'data': {}})
            # else:
            # self.__load_job_list_result = False
            # bm = self._manager(batchmanager)
            #                 view = self._get_manager_view(bm)
            #                 bm.loadJobList()
            #                 logger.debug("async loading finished... return result")
            #
            #                 kwargs.update({'encode_json': True})
            #                 kwargs.update({'reverse': reverse})
            #                 if only_modified:
            #                     data = view.getModifiedJobs(**kwargs)
            #                 else:
            #                     data = view.getJobs(**kwargs)
            #                 return '%s,"data":%s}' % (json.dumps({"success": True})[:-1], data)
            #
            #         bm = self._manager(batchmanager)
            #         view = self._get_manager_view(bm)
            #         if not bm.hasJobListLoaded():
            #             logger.debug("job list not loaded: starting async request")
            #             async_load_job_list = rpyc.async(bm.loadJobList)
            #             self.__load_job_list_result = async_load_job_list()
            #             return json.dumps({'success': False, 'data': {}})
            #         else:
            #             logger.debug("job list already loaded, update job list and return result")
            #             bm.loadJobList()
            #
            #             kwargs.update({'encode_json': True})
            #             kwargs.update({'reverse': reverse})
            #             if only_modified:
            #                 data = view.getModifiedJobs(**kwargs)
            #             else:
            #                 data = view.getJobs(**kwargs)
            #             return '%s,"data":%s}' % (json.dumps({"success": True})[:-1], data)

    @cherrypy.expose
    @cherrypy.tools.ajax(encoded=True)
    @cherrypy.tools.json_parameters()
    def get_modified_job_data(self, batchmanager=None, **kwargs):
        data = self.get_job_data(batchmanager,
                                 only_modified=True, **kwargs)
        return data

    # @cherrypy.expose
    # def isBatchManagerAvailable(self):
    # try:
    # return self.success({"data": True})
    # except Exception, e:
    # return self.fail("Couldn't get BatchManager availability!<br />\
    # Reason: %s" % str(e))

    @cherrypy.expose
    def set_batch_manager_output_folder(self, foldername, batchmanager=None):
        if batchmanager is None:
            batchmanager = self.__defaultmanager
        bm = self._manager(batchmanager)
        bm.setDataPath(os.path.join(foldername, batchmanager))
        return 0

    @cherrypy.expose
    def get_number_of_jobs(self, manager=None, status="All Jobs", command=""):
        if status == "All Jobs" or status == "":
            return {'number': self._manager(manager).getNumberOfJobs(command)}
        else:
            return {'number': self._manager(manager).getNumberOfJobsWithStatus(status, command)}

    @cherrypy.expose
    def get_number_of_jobs_by_status(self, manager=None):
        jobs_by_status = self._manager(manager).getNumberOfJobsByStatus()
        reply = {'total': jobs_by_status[0],
                 'normally': jobs_by_status[1],
                 'abnormally': jobs_by_status[2],
                 'running': jobs_by_status[3],
                 'pending': jobs_by_status[4],
                 'suspended': jobs_by_status[5]}
        return reply

    """##########################Job Designer############################"""

    @cherrypy.expose
    def create_command_line_designer(self):
        try:
            self._cmd_line_designer = vispa.workspace.get_instance("batchsystemmanager.commandline.CommandLineDesigner")
            return 0
        except Exception, e:
            logger.debug("Couldn't get the CommandLineDesigner RPC process! <br />Reason: %s" % str(e))

    @cherrypy.expose
    @cherrypy.tools.json_parameters()
    def add_option(self, name="", option_string="", active=True, value="", group="", decorator=None):
        if group == "":
            option_group = "default"
        else:
            option_group = group
        self._cmd_line_designer.addOption(name, option_string, active, value, option_group, decorator)
        return self._cmd_line_designer.checkIfAllOptionsActive()

    @cherrypy.expose
    @cherrypy.tools.json_parameters()
    def add_options(self,options):
        for option in options:
            name=option[0]
            option_string=option[1]
            active=option[2]
            value=option[3]
            group=option[4]
            decorator=option[5]
            self._cmd_line_designer.addOption(name,option_string,active,value,group,decorator)
        return self._cmd_line_designer.checkIfAllOptionsActive()

    @cherrypy.expose
    def get_options(self):
        options = self._cmd_line_designer.getOptions()
        reply = []
        for option in options:
            if option.getDecorator() is None:
                decorator_name = ""
            else:
                decorator_name = option.getDecorator().getName()
            reply.append({
                "name": option.getName(),
                "optionString": option.getOptionString(),
                "value": option.getValue(),
                "group": option.getGroup(),
                "decorator": decorator_name,
                "active": option.isActive()
            })
        return {"data": reply}

    @cherrypy.expose
    @cherrypy.tools.json_parameters()
    def get_option(self, option_name):
        option = self._cmd_line_designer.getOptionByName(option_name)
        if option.getDecorator() is None:
            decorator_name = ""
        else:
            decorator_name = option.getDecorator().getName()
        reply = {
            "name": option.getName(),
            "optionString": option.getOptionString(),
            "value": option.getValue(),
            "group": option.getGroup(),
            "decorator": decorator_name,
            "active": option.isActive()
        }
        return reply

    @cherrypy.expose
    @cherrypy.tools.json_parameters()
    def remove_option(self, option_name):
        option = self._cmd_line_designer.getOptionByName(option_name)
        self._cmd_line_designer.removeOption(option)
        return self._cmd_line_designer.checkIfAllOptionsActive()

    @cherrypy.expose
    @cherrypy.tools.json_parameters()
    def modify_option(self, name, key_string, value_string):
        keys = key_string
        values = value_string
        for i in range(0, len(keys)):
            self._cmd_line_designer.modifyOption(optionname=name, key=keys[i], value=values[i])

    @cherrypy.expose
    @cherrypy.tools.json_parameters()
    def set_option_active(self, option_name, status):
        option = self._cmd_line_designer.getOptionByName(option_name)
        option.setActive(status)
        return self._cmd_line_designer.checkIfAllOptionsActive()

    @cherrypy.expose
    @cherrypy.tools.json_parameters()
    def set_all_options_active(self, status):
        options = self._cmd_line_designer.getOptions()
        for option in options:
            option.setActive(status)

    @cherrypy.expose
    @cherrypy.tools.json_parameters()
    def check_if_option_name_is_unique(self, name):
        for option in self._cmd_line_designer.getOptions():
            if option.getName() == name:
                return {"is_unique": False}
        return {"is_unique": True}

    @cherrypy.expose
    def get_options_sorted_by_group(self):
        options = self._cmd_line_designer.getOptions()
        groups = self._get_list_of_groups()
        reply = []
        group_number=0
        option_number=3
        for group in groups:
            group_number+=2
            first_element = True
            for option in options:
                if option.getGroup() == group:
                    if first_element:
                        if option.getDecorator() is None:
                            decorator_name = ""
                        else:
                            decorator_name = option.getDecorator().getName()
                        reply.append({
                            "name": option.getName(),
                            "optionString": option.getOptionString(),
                            "value": option.getValue(),
                            "group": option.getGroup(),
                            "decorator": decorator_name,
                            "active": option.isActive(),
                            "renderGroup": True,
                            "optionNumber": option_number,
                            "groupNumber": group_number
                        })
                        option_number+=2
                        first_element = False
                    else:
                        if option.getDecorator() is None:
                            decorator_name = ""
                        else:
                            decorator_name = option.getDecorator().getName()
                        reply.append({
                            "name": option.getName(),
                            "optionString": option.getOptionString(),
                            "value": option.getValue(),
                            "group": option.getGroup(),
                            "decorator": decorator_name,
                            "active": option.isActive(),
                            "renderGroup": False,
                            "optionNumber": option_number,
                            "groupNumber": group_number
                        })
        return {"data": reply}

    @cherrypy.expose
    @cherrypy.tools.json_parameters()
    def remove_option_group(self, group_name):
        options = self._cmd_line_designer.getOptions()
        remove_options_list = []
        for option in options:
            if option.getGroup() == group_name:
                remove_options_list.append(option)
        self._cmd_line_designer.removeOptions(remove_options_list)
        return self._cmd_line_designer.checkIfAllOptionsActive()

    @cherrypy.expose
    @cherrypy.tools.json_parameters()
    def add_parameter_range(self, name, active=True, values_string="", script=None):
        values = []
        if len(values_string) > 0:
            values_conversion_success = False
            try:
                values_array = values_string.split(",")
                for value in values_array:
                    values.append(float(value))
                values_conversion_success = True
            except:
                pass
            if not values_conversion_success:
                try:
                    values_array = values_string.split(" ")
                    for value in values_array:
                        values.append(float(value))
                    values_conversion_success = True
                except:
                    pass
            if not values_conversion_success:
                try:
                    values_array = values_string.split(", ")
                    for value in values_array:
                        values.append(float(value))
                except:
                    pass
        self._cmd_line_designer.addParameterRange(name=name, active=active, values=values, script=script)
        return self._cmd_line_designer.checkIfAllRangesActive()

    @cherrypy.expose
    def get_parameter_ranges(self):
        ranges = self._cmd_line_designer.getParameterRanges()
        reply = []
        for parameter_range in ranges:
            try:
                values = map(float, parameter_range.getValues())
            except Exception:
                values = "Error"
            reply.append({
                "name": parameter_range.getName(),
                "values": values,
                "script": parameter_range.getScript(),
                "active": parameter_range.isActive()
            })
        return {"data": reply}

    @cherrypy.expose
    @cherrypy.tools.json_parameters()
    def get_parameter_range(self, index):
        parameter_range = self._cmd_line_designer.getParameterRanges()[int(index)]
        try:
            values = map(float, parameter_range.getValues())
        except Exception:
            values = "Error"
        reply = {
            "name": parameter_range.getName(),
            "values": values,
            "script": parameter_range.getScript(),
            "active": parameter_range.isActive()
        }
        return reply

    @cherrypy.expose
    def get_parameter_range_names(self):
        ranges = self._cmd_line_designer.getParameterRanges()
        reply = []
        for parameter_range in ranges:
            reply.append({
                "range-name": parameter_range.getName()
            })
        return reply

    @cherrypy.expose
    @cherrypy.tools.json_parameters()
    def remove_parameter_range(self, index):
        self._cmd_line_designer.removeParameterRange(self._cmd_line_designer.getParameterRanges()[int(index)])
        return self._cmd_line_designer.checkIfAllRangesActive()

    @cherrypy.expose
    @cherrypy.tools.json_parameters()
    def modify_parameter_range(self, name, key_string, value_string):
        for i in range(0, len(key_string)):
            if key_string[i]=="values":
                values = []
                if len(value_string[i]) > 0:
                    values_conversion_success = False
                    try:
                        values_array = value_string[i].split(",")
                        for value in values_array:
                            values.append(float(value))
                        values_conversion_success = True
                    except:
                        pass
                    if not values_conversion_success:
                        try:
                            values_array = value_string[i].split(" ")
                            for value in values_array:
                                values.append(float(value))
                            values_conversion_success = True
                        except:
                            pass
                    if not values_conversion_success:
                        try:
                            values_array = value_string[i].split(", ")
                            for value in values_array:
                                values.append(float(value))
                        except:
                            pass
                    self._cmd_line_designer.modifyParameterRange(name=name, key=key_string[i], value=values)
            else:
                self._cmd_line_designer.modifyParameterRange(name=name, key=key_string[i], value=value_string[i])

    @cherrypy.expose
    @cherrypy.tools.json_parameters()
    def set_parameter_range_active(self, index, status):
        parameter_range = self._cmd_line_designer.getParameterRanges()[int(index)]
        parameter_range.setActive(status)
        return self._cmd_line_designer.checkIfAllRangesActive()

    @cherrypy.expose
    @cherrypy.tools.json_parameters()
    def set_all_parameter_ranges_active(self, status):
        for parameter_range in self._cmd_line_designer.getParameterRanges():
            parameter_range.setActive(status)

    @cherrypy.expose
    @cherrypy.tools.json_parameters()
    def add_decorator(self, name, script=""):
        self._cmd_line_designer.addOptionDecorator(name=name, script=script)

    @cherrypy.expose
    def get_decorators(self):
        decorators = self._cmd_line_designer.getOptionDecorators()
        reply = []
        for decorator in decorators:
            reply.append({
                "name": decorator.getName(),
                "script": decorator.getScript()
            })
        return {"data": reply}

    @cherrypy.expose
    def get_decorator_names(self):
        decorators = self._cmd_line_designer.getOptionDecorators()
        reply = [{
            "decoratorname": "None"
            }]
        for decorator in decorators:
            reply.append({
                "decoratorname": decorator.getName()
            })
        return reply

    @cherrypy.expose
    @cherrypy.tools.json_parameters()
    def get_decorator(self, index):
        decorator = self._cmd_line_designer.getOptionDecorators()[int(index)]
        reply = {
            "name": decorator.getName(),
            "script": decorator.getScript()
        }
        return reply

    @cherrypy.expose
    @cherrypy.tools.json_parameters()
    def remove_decorator(self, index):
        self._cmd_line_designer.removeOptionDecorator(self._cmd_line_designer.getOptionDecorators()[int(index)])

    @cherrypy.expose
    @cherrypy.tools.json_parameters()
    def modify_decorator(self, name, key_string, value_string):
        keys = key_string
        values = value_string
        for i in range(0, len(keys)):
            self._cmd_line_designer.modifyOptionDecorator(name, keys[i], values[i])

    @cherrypy.expose
    @cherrypy.tools.json_parameters()
    def check_if_decorator_name_is_unique(self, decorator_name):
        for decorator in self._cmd_line_designer.getOptionDecorators():
            if decorator.getName() == decorator_name:
                return {"is_unique": False}
        return {"is_unique": True}

    @cherrypy.expose
    def get_command_lines(self):
        try:
            commandlines = self._cmd_line_designer.buildCommandLines()
        except Exception as e:
            raise AjaxException(str(e).split("=========")[0])
        return {
            "commandlines": map(str, commandlines)
        }

    @cherrypy.expose
    @cherrypy.tools.json_parameters()
    def set_command(self, command_string=""):
        self._cmd_line_designer.setCommand(command_string)

    @cherrypy.expose
    def get_command(self):
        command = self._cmd_line_designer.getCommand()
        return {"command": command}

    @cherrypy.expose
    def get_number_of_designer_jobs(self):
        return {"numberOfJobs": self._cmd_line_designer.getNumberOfJobs()}

    @cherrypy.expose
    @cherrypy.tools.json_parameters()
    def test_script(self, script):
        text = script
        text = text.lstrip()
        text = text.rstrip()
        text = "\n" + text
        cmd = "def userScript():\n"
        cmd += "\n  ".join(text.split("\n"))

        try:
            exec cmd
            data = userScript()
            return {"response": data,"successfull":True}
        except Exception as e:
            return {"response": str(e),"successfull":False}

    def _get_list_of_groups(self):
        options = self._cmd_line_designer.getOptions()
        group_list = ["default"]
        for option in options:
            if not option.getGroup() in group_list:
                group_list.append(option.getGroup())
        return group_list

    @cherrypy.expose
    def get_list_of_groups(self):
        groups = self._get_list_of_groups()
        reply = []
        for group in groups:
            reply.append({"groupname": group})

    @cherrypy.expose
    @cherrypy.tools.json_parameters()
    def save_designer_jobs(self, path):
        self._cmd_line_designer.saveAs(path=path)

    @cherrypy.expose
    @cherrypy.tools.json_parameters()
    def load_designer_jobs(self, path):
        self._cmd_line_designer.loadFromFile(path=path)
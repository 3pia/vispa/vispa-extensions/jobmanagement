# -*- coding: utf-8 -*-

import batchsystemmanager

from vispa.server import AbstractExtension
from .controller import JobmanagementController
import vispa


class JobmanagementExtension(AbstractExtension):
    def __init__(self, server):
        AbstractExtension.__init__(self, server)

    def name(self):
        return 'jobmanagement'

    def dependencies(self):
        return []

    def setup(self):
        controller = JobmanagementController(extension=self)
        self.add_controller(controller)
        vispa.workspace.add_package_files(batchsystemmanager)

#         self.add_js('js/extension.js')
#         self.add_js('js/jobdashboard/dashboard.js')
#         self.add_js('js/jobdesigner/designer.js')
#         self.add_css('css/jobdashboard.css')
#         self.add_css('css/jobdesigner.css')

        """ job submission """
#         self.add_js("js/jobsubmission/submission.js")
#         self.add_css("css/jobsubmission.css")
#         self.add_css("css/jobextension_nodes.css")

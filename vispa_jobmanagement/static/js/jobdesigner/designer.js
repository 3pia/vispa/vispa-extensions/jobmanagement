/*global vispa */
/*globals vispa*/
require.config({
    shim: {
        "extensions/jobmanagement/static/vendor/treegrid/js/jquery.treegrid": ["jquery"]
    }
});

define(["jquery",
  "jclass",
  "vispa/views/main",
  "require",
  "async",
  "text!../../html/jobdesigner.html",
  "text!../../html/new_designer_option_dialog.html",
  "text!../../html/new_designer_range.html",
  "text!../../html/new_designer_decorator_dialog.html",
  "css!../../css/jobdesigner",
  "extensions/jobmanagement/static/vendor/treegrid/js/jquery.treegrid",
  "css!../../css/jobextension_nodes",
  "css!../../vendor/treegrid/css/jquery.treegrid.css"],function($,JClass,MainView,require,async,jobdesignerTemplate,newDesignerOptionTemplate,newDesignerRangeTemplate,newDesignerDecoratorTemplate) {
    var JobDesignerView = MainView._extend({
      init: function init(args) {
        var self = this;
        init._super.apply(this,arguments)
      },
      render: function(node){
        this.renderContent(node);
      },
      renderContent: function (node) {
        var self = this;
        self.setLoading(true);
        self.POST("create_command_line_designer",function (err,tmp) {
          self.designerNode = $(jobdesignerTemplate).appendTo(node);
          self.commands=$(".designer-command",self.designerNode);
          self.numberOfJobs=$(".number-of-jobs",self.designerNode);
          self.previewNode=self.designerNode.find("#preview-ace");
          self.allOptionsCheckbox=$(".all-options-checkbox",self.designerNode);
          self.allRangesCheckbox=$(".all-ranges-checkbox",self.designerNode);
          $(".options-table",self.designerNode).treegrid();
          require(["ace/ace", "ace/ext/language_tools"],function(ace){
            self.preview=ace.edit(self.previewNode.get(0));
            self.preview.setTheme("ace/theme/textmate");
            self.preview.getSession().setMode("ace/mode/text");
            self.preview.setReadOnly(true);
            self.updateRanges(false);
            self.updateDecorators(false);
            self.updateOptions(true);
            self.setLoading(false);
          });
          self.allOptionsCheckbox.click(function(){
            self.setAllOptionsActive();
          });
          self.allRangesCheckbox.click(function(){
            self.setAllRangesActive();
          });
          $(".add-option", self.designerNode).click(function () {
            self.newOption();
          });
          $(".options-table",self.designerNode).on("click",".options-active",function(){
            var optionName = $(this).prop("optionName");
            self.setOptionActive(optionName,$(this).prop("checked"));
          });
          $(".options-table", self.designerNode).on("click", ".remove-option", function () {
            var optionName = $(this).prop("optionName");
            self.removeOption(optionName);
          });
          $(".options-table", self.designerNode).on("click", ".edit-option", function () {
            var optionName = $(this).prop("optionName");
            self.editOption(optionName);
          });
          $(".options-table",self.designerNode).on("click", ".remove-group",function(){
            var groupName=$(this).prop("groupName");
            self.removeOptionGroup(groupName);
          });
          $(".new-range", self.designerNode).click(function () {
            self.newRange();
          });
          $(".ranges-table",self.designerNode).on("click",".ranges-active",function(){
            var rowIndex = $(this).closest("tr")[0].rowIndex - 1;
            self.setRangeActive(rowIndex,$(this).prop("checked"));
          });
          $(".ranges-table", self.designerNode).on("click", ".remove-range", function () {
            var rowIndex = $(this).closest("tr")[0].rowIndex - 1;
            self.removeRange(rowIndex)
          });
          $(".ranges-table", self.designerNode).on("click", ".edit-range", function () {
            var rowIndex = $(this).closest("tr")[0].rowIndex - 1;
            self.editRange(rowIndex);
          });
          $(".new-decorator", self.designerNode).click(function () {
            self.newDecorator();
          });
          $(".decorator-table", self.designerNode).on("click", ".remove-decorator", function () {
            var rowIndex = $(this).closest("tr")[0].rowIndex;
            self.removeDecorator(rowIndex);
          });
          $(".decorator-table", self.designerNode).on("click", ".edit-decorator", function () {
            var rowIndex = $(this).closest("tr")[0].rowIndex;
            self.editDecorator(rowIndex);
          });
          $(".designer-submit",self.designerNode).click(function(){
            self.submit();
          });
          $(".refresh-preview",self.designerNode).click(function(){
            self.updatePreview(true);
          });
          $(".designer-save",self.designerNode).click(function(){
            self.saveDesignerJobs();
          });
          $(".designer-load",self.designerNode).click(function(){
            self.loadDesignerJobs();
          });
          $(".help-icon",self.designerNode).popover();
          var parameters=self.getState("options");
          if(parameters!=null) {
            var optionName;
            var optionValue;
            var moduleNames = _.keys(parameters);
            var options=[]
            var j = 0;
            $.each(parameters, function () {
              var currentModuleName=moduleNames[j];
              for (var i = 0; i < this.length; i++) {
                optionName=String(currentModuleName)+"_"+this[i].key;
                optionValue=this[i].value;
                options.push([optionName,null,false,optionValue,currentModuleName,null]);
              }
              j += 1
            });
            self.POST("add_options",JSON.stringify({options: options}),function(err,ret){
              self.updateOptions(true);
              self.allOptionsCheckbox.prop("checked",ret);
            });
          }
        });
      },
      newOption: function () {
        var self = this;
        self.GET("get_decorator_names",function (err,res) {
          self.dialog({
            title: "New Option",
            body: newDesignerOptionTemplate,
            buttons:{
              cancel: {
                label: "Cancel"
              },
              okay:{
                label: "OK",
                class: "btn btn-primary",
                callback: function(){
                  var dialogWindow=this;
                  var name = $(".new-option-name", dialogWindow.$body).val();
                  var optionString = $(".new-option-string", dialogWindow.$body).val();
                  var value = $(".new-option-value", dialogWindow.$body).val();
                  var decoratorSelector = $(".new-option-decorator", this.$body);
                  var decorator = decoratorSelector.val();
                  var group=$(".new-option-group",dialogWindow.$body).val();
                  var checkIfNameIsUnique=self.GET("check_if_option_name_is_unique",JSON.stringify({name: name}),function(err,res){
                    if(res.is_unique) {
                      self.POST("add_option", JSON.stringify({
                        name: name,
                        option_string: optionString,
                        active: true,
                        value: value,
                        group: group,
                        decorator: decorator
                      }),function (err,res) {
                        self.allOptionsCheckbox.prop("checked",res);
                        dialogWindow.close();
                        self.updateOptions(true);
                      });
                    }
                    else{
                      $(".new-job-option-name-already-exists",dialogWindow.$footer).show().delay(3000).fadeOut();
                    }
                  });
                }
              }
            },
            onOpen: function () {
              var dialogWindow = this;
              var decoratorSelector = $(".new-option-decorator", this.$body);
              decoratorSelector.render(res);
              decoratorSelector.selectpicker();
              $(".job-designer-help-icon",dialogWindow).popover();
            }
          });
        });
      },

      editOption: function (optionName) {
        var self = this;
        var tasks=[];
        tasks.push(self.GET.bind(self,"get_option", JSON.stringify({
          option_name: optionName
        })));
        tasks.push(self.GET.bind(self,"get_decorator_names"));
        async.parallel(tasks,function (err,ret) {
          var res=ret[0];
          var decorators=ret[1];
          self.dialog({
            title: "Edit Option",
            body: newDesignerOptionTemplate,
            buttons: {
              cancel: {
                label: "Cancel"
              },
              okay: {
                label: "Okay",
                class: "btn btn-primary",
                callback: function(){
                  var dialogWindow=this;
                  var optionName = $(".new-option-name", dialogWindow.$body);
                  var optionString = $(".new-option-string", dialogWindow.$body);
                  var optionValue = $(".new-option-value", dialogWindow.$body);
                  var optionGroup=$(".new-option-group",dialogWindow.$body);
                  var decoratorSelector = $(".new-option-decorator", dialogWindow.$body);
                  var keyArray = [];
                  var valueArray = [];
                  if (optionString.val() != res[0].optionString) {
                    keyArray[0] = "optionString";
                    valueArray[0] = optionString.val();
                  }
                  if (optionValue.val() != res[0].value) {
                    keyArray[keyArray.length] = "value";
                    valueArray[valueArray.length] = optionValue.val();
                  }
                  if (decoratorSelector.val() != res[0].decorator) {
                    keyArray[keyArray.length] = "decorator";
                    valueArray[valueArray.length] = decoratorSelector.val();
                  }
                  if(optionGroup.val() != res[0].group){
                    keyArray[keyArray.length] = "group";
                    valueArray[valueArray.length] = optionGroup.val();
                  }
                  if (keyArray.length != 0) {
                    self.POST("modify_option", JSON.stringify({
                      name: res[0].name,
                      key_string: keyArray,
                      value_string: valueArray
                    }),function () {
                      if (optionName.val() != res[0].name) {
                        self.GET("check_if_option_name_is_unique",JSON.stringify({name: optionName.val()}),function(err,ret) {
                          if (ret.is_unique) {
                            self.POST("modify_option", JSON.stringify({
                              name: res[0].name,
                              key_string: ["name"],
                              value_string: [optionName.val()]
                            }), function () {
                              self.updateOptions(true);
                            });
                          } else {
                            $(".new-job-option-name-already-exists",dialogWindow.$footer).show().delay(3000).fadeOut();
                          }
                        });
                      }
                      else {
                        self.updateOptions(true);
                      }
                    });
                  }
                  else {
                    if (optionName.val() != res[0].name) {
                      self.GET("check_if_option_name_is_unique",JSON.stringify({name: optionName.val()}),function(err,ret) {
                        if (ret.is_unique) {
                          self.POST("modify_option", JSON.stringify({
                            name: res[0].name,
                            key_string: ["name"],
                            value_string: [optionName.val()]
                          }), function () {
                            dialogWindow.close();
                            self.updateOptions(true);
                          });
                        } else {
                          $(".new-job-option-name-already-exists",dialogWindow.$footer).show().delay(3000).fadeOut();
                        }
                      });
                    }
                  }

                }
              }
            },
            onOpen: function () {
              var dialogWindow = this;
              var optionName = $(".new-option-name", dialogWindow.$body);
              var optionString = $(".new-option-string", dialogWindow.$body);
              var optionValue = $(".new-option-value", dialogWindow.$body);
              var optionGroup=$(".new-option-group",dialogWindow.$body);
              var decoratorSelector = $(".new-option-decorator", dialogWindow.$body);
              $(".job-designer-help-icon",dialogWindow.$body).popover();
              decoratorSelector.render(decorators[0]);
              decoratorSelector.selectpicker();
              decoratorSelector.selectpicker("val", res[0].decorator);
              optionName.val(res[0].name);
              optionString.val(res[0].optionString);
              optionValue.val(res[0].value);
              optionGroup.val(res[0].group);
              $(".new-option-cancel", dialogWindow.$footer).click(function () {
                dialogWindow.close();
              });
            }
          })
        });
      },

        updateOptions: function (previewUpdate) {
            var self = this;
            self.setLoading(true);
            self.GET("get_options_sorted_by_group",function (err,res) {
                var directives = {
                    "group-name":{
                        text: function(){
                            if(this.renderGroup==true) {
                                return this.group;
                            }
                            else{
                                return;
                            }
                        },
                    },
                    "options-table-group-row":{
                        class: function(){
                            if(this.renderGroup==true){
                                return "treegrid-"+String(this.groupNumber);
                            }
                            else{
                                return;
                            }
                        }
                    },
                    "options-table-row":{
                        class: function(){
                            return "treegrid-"+String(this.optionNumber)+" treegrid-parent-"+String(this.groupNumber);
                        }
                    },
                    "options-active": {
                        checked: function(){
                            return this.active;
                        },
                        optionName: function(){
                            return this.name;
                        }
                    },
                    "options-name": {
                        text: function () {
                            return this.name;
                        },
                        value: function(){
                            return this.name;
                        }
                    },
                    "options-default-value": {
                        text: function () {
                            return this.value;
                        }
                    },
                    "options-decorator":{
                        text: function(){
                            return this.decorator;
                        }
                    },
                    "edit-option": {
                        optionName: function(){
                            return this.name;
                        }
                    },
                    "remove-option":{
                        optionName: function(){
                            return this.name;
                        }
                    },
                    "remove-group":{
                        groupName:function(){
                            if (this.renderGroup == true) {
                                return this.group;
                            }
                            else {
                                return;
                            }
                        },
                        style: function(){
                            if (this.renderGroup==true){
                                return "cursor: pointer";
                            }
                            else{
                                return "display: none";
                            }
                        }
                    },
                    "options-show-cell":{
                        style: function(){
                            return "display: inline";
                        }
                    }
                }
                $(".options-table-body", self.designerNode).render(res.data, directives);
                $(".options-table",self.designerNode).treegrid();
                self.setLoading(false);
            });
            if(previewUpdate===true) {
                self.updatePreview(true);
            }
        },
        removeOption: function (name) {
            var self = this;
            self.POST("remove_option", JSON.stringify({
                option_name: name
            }),function(err,res){
                self.allOptionsCheckbox.prop("checked",res);
                self.updateOptions(true)
            });
        },

        setOptionActive: function(name,status){
          var self=this;
          self.POST("set_option_active",JSON.stringify({
            option_name: name,
            status: status
          }),function(err,res){
            self.allOptionsCheckbox.prop("checked",res);
            self.updatePreview(true);
          });
        },

        setAllOptionsActive: function(){
          var self=this;
          var status=self.allOptionsCheckbox.prop("checked");
          self.POST("set_all_options_active",JSON.stringify({"status":status}),function(){
            $(".options-active",self.designerNode).prop("checked",status);
            self.updatePreview(false);
          })
        },

        removeOptionGroup: function(name){
          var self=this;
          self.confirm("Are you sure you want to remove all options in the group "+String(name)+" ?",function(){
            var removeGroup=self.POST("remove_option_group",JSON.stringify({"group_name":name}),function(err,res){
              self.updateOptions(true);
              dialogWindow.close();
              self.allOptionsCheckbox.prop("checked",res);
            });
          });
        },

        newRange: function () {
          var self = this;
          self.dialog({
            title: "New Parameter Range",
            body: newDesignerRangeTemplate,
            buttons: {
              cancel: {
                label: "Cancel"
              },
              okay: {
                label: "Okay",
                class: "btn btn-primary",
                callback: function () {
                  var dialogWindow = this;
                  var valuesInput = $(".new-range-values", dialogWindow.$body);
                  var errorDisplay = $(".new-job-range-cannot-execute", dialogWindow.$footer);
                  var name = $(".new-range-name", dialogWindow.$body).val();
                  var script = dialogWindow.scriptInput.getValue();
                  var values = valuesInput.val();
                  if (dialogWindow.scriptInput.getValue().length > 0) {
                    self.POST("test_script", JSON.stringify({script: script}), function (err, ret) {
                      if (ret.successfull == true) {
                        self.POST("add_parameter_range", JSON.stringify({
                          name: name,
                          script: script,
                          values_string: values
                        }), function (err, res) {
                          dialogWindow.close();
                          self.allRangesCheckbox.prop("checked", res);
                          self.updateRanges(true);
                          self.updateDecorators();
                        });
                      }
                      else {
                        errorDisplay.show();
                        $(".new-job-range-error-message", dialogWindow.$footer).text(ret.response);
                      }
                    });
                  } else {
                    self.POST("add_parameter_range", JSON.stringify({
                      name: name,
                      script: script,
                      values_string: values
                    }), function (err, res) {
                      dialogWindow.close();
                      self.allRangesCheckbox.prop("checked", res);
                      self.updateRanges(true);
                      self.updateDecorators();
                    });
                  }
                }
              }
            },
            onOpen: function () {
              var dialogWindow = this;
              var valuesInput = $(".new-range-values", dialogWindow.$body);
              var errorDisplay = $(".new-job-range-cannot-execute", dialogWindow.$footer);
              $(".job-designer-help-icon", dialogWindow.$body).popover();
              dialogWindow.scriptInputNode = $("#new-range-script-ace", dialogWindow.$body);
              require(["ace/ace", "ace/ext/language_tools"], function (ace) {
                dialogWindow.scriptInput = ace.edit(dialogWindow.scriptInputNode.get(0));
                dialogWindow.scriptInput.setTheme("ace/theme/textmate");
                dialogWindow.scriptInput.getSession().setMode("ace/mode/text");
                dialogWindow.scriptInput.on("change", function () {
                  errorDisplay.hide();
                });
              });
              $(".test-script-button", dialogWindow.$body).click(function () {
                if (dialogWindow.scriptInput.getValue().length > 0) {
                  self.POST("test_script", JSON.stringify(
                    {script: dialogWindow.scriptInput.getValue()}), function (err, res) {
                    if (res.successfull == true) {
                      valuesInput.val(res.response);
                    }
                    else {
                      errorDisplay.show();
                      $(".new-job-range-error-message", dialogWindow.$footer).text(res.response);
                    }
                  });
                }
              });
            }
          });
        },

        updateRanges: function (previewUpdate) {
            var self = this;
            self.setLoading(true);
                self.POST("get_parameter_ranges",function (err,res) {
                var directives = {
                    "ranges-active":{
                        checked: function(){
                            return this.active;
                        }
                    },
                    "ranges-name": {
                        text: function () {
                            return this.name;
                        }
                    },
                    "ranges-values": {
                        text: function () {
                            return this.values;
                        }
                    },
                    "ranges-script": {
                        text: function () {
                            return this.script;
                        }
                    },
                    "ranges-icons": {
                        style: function() {
                        return "display: inline";
                    }
                }
                };
                $(".ranges-table-body", self.designerNode).render(res.data, directives);
                    self.setLoading(false);
            });
            self.GET("get_number_of_designer_jobs",function(err,res){
                self.numberOfJobs.text(res.numberOfJobs);
            });
            if(previewUpdate===true) {
                self.updatePreview(true);
                }
        },

        removeRange: function (index) {
            var self = this;
            self.POST("remove_parameter_range", JSON.stringify({
                index: index
            }),function (err,res) {
                self.allRangesCheckbox.prop("checked",res);
                self.updateRanges(true);
                self.updateDecorators();
            });
        },

        editRange: function (index) {
            var self = this;
          self.GET("get_parameter_range",JSON.stringify({
            index: index
          }),function (err,res) {
            self.dialog({
              title: "Edit Parameter Range",
              body: newDesignerRangeTemplate,
              buttons:{
                cancel: {
                  label: "Cancel"
                },
                okay: {
                label: "Okay",
                class: "btn btn-primary",
                callback: function () {
                  var dialogWindow=this;
                  var rangeName = $(".new-range-name", dialogWindow.$body);
                  var rangeValues = $(".new-range-values", dialogWindow.$body);
                  var errorDisplay=$(".new-job-range-cannot-execute",dialogWindow.$footer);
                  var keyArray = [];
                  var valueArray = [];
                  self.POST("test_script",JSON.stringify({script: dialogWindow.scriptInput.getValue()}),function(err,ret){
                    if (ret.successfull==true || dialogWindow.scriptInput.getValue().length==0) {
                      if (dialogWindow.scriptInput.getValue() != res.script) {
                        keyArray[0] = "script";
                        valueArray[0] = dialogWindow.scriptInput.getValue();
                      }
                      if (rangeValues.val() != res.values) {
                        keyArray[keyArray.length] = "values";
                        valueArray[valueArray.length] = rangeValues.val();
                      }
                      if (keyArray.length > 0) {
                        self.POST("modify_parameter_range", JSON.stringify({
                          name: res.name,
                          key_string: keyArray,
                          value_string: valueArray
                        }), function (err) {
                          if (rangeName.val() != res.name) {
                            var editName1 = self.POST("modify_parameter_range", JSON.stringify({
                              name: res.name,
                              key_string: ["name"],
                              value_string: [rangeName.val()]
                            }), function (err) {
                              dialogWindow.close();
                              self.updateRanges(true);
                            });
                          } else {
                            dialogWindow.close();
                            self.updateRanges(true);
                          }
                        });
                      } else {
                        if (rangeName.val() != res.name) {
                          self.POST("modify_parameter_range", JSON.stringify({
                            name: res.name,
                            key_string: ["name"],
                            value_string: [rangeName.val()]
                          }), function () {
                            dialogWindow.close();
                            self.updateRanges(true);
                          })
                        } else {
                          dialogWindow.close();
                        }
                      }
                    }
                    else{
                      errorDisplay.show();
                      $(".new-job-range-error-message",dialogWindow.$footer).text(ret.response);
                    }
                  });
                }
              }
              },
              onOpen: function () {
                var dialogWindow = this;
                $(".new-range-cancel", dialogWindow.$footer).click(function () {
                  dialogWindow.close();
                });
                $(".job-designer-help-icon",dialogWindow.$body).popover();
                var rangeName = $(".new-range-name", dialogWindow.$body);
                dialogWindow.rangeScriptNode = $("#new-range-script-ace",dialogWindow.$body);
                var rangeValues = $(".new-range-values", dialogWindow.$body);
                var errorDisplay=$(".new-job-range-cannot-execute",dialogWindow.$footer);
                require(["ace/ace", "ace/ext/language_tools"],function(ace){
                  dialogWindow.scriptInput=ace.edit(dialogWindow.rangeScriptNode.get(0));
                  dialogWindow.scriptInput.setTheme("ace/theme/textmate");
                  dialogWindow.scriptInput.getSession().setMode("ace/mode/text");
                  dialogWindow.scriptInput.setValue(res.script,1);
                  dialogWindow.scriptInput.on("change",function(){
                    errorDisplay.hide();
                  });
                });
                rangeName.val(res.name);
                rangeValues.val(res.values);
                $(".test-script-button",dialogWindow.$body).click(function(){
                  if(dialogWindow.scriptInput.getValue().length>0) {
                    self.POST("test_script", JSON.stringify({script: dialogWindow.scriptInput.getValue()}),function (err,res) {
                      if (res.successfull==true) {
                        rangeValues.val(res.response);
                      }
                      else{
                        errorDisplay.show();
                        $(".new-job-range-error-message",dialogWindow.$footer).text(res.response);
                      }
                    });
                  }
                });
              }
            })
          });
        },

        setRangeActive: function(index,status){
            var self=this;
            self.POST("set_parameter_range_active",JSON.stringify({
                index:index,
                status:status
            }),function(err,res){
                self.allRangesCheckbox.prop("checked",res);
                self.GET("get_number_of_designer_jobs",function(err,res){
                    self.numberOfJobs.text(res.numberOfJobs);
                });
                self.updatePreview(true);
            });
        },

        setAllRangesActive: function(){
            var self=this;
            var status=self.allRangesCheckbox.prop("checked");
            self.POST("set_all_parameter_ranges_active",JSON.stringify({"status":status}),function(err){
                $(".ranges-active",self.designerNode).prop("checked",status);
                self.updatePreview();
            });
        },

        newDecorator: function () {
          var self = this;
          self.GET("get_parameter_range_names",function (err,rangeNames) {
            self.dialog({
              title: "New Decorator",
              body: newDesignerDecoratorTemplate,
              buttons: {
                cancel: {
                  label: "Cancel"
                },
                okay: {
                  label: "Okay",
                  class: "btn btn-primary",
                  callback: function(){
                    var dialogWindow=this;
                    var name = $(".new-decorator-name", dialogWindow.$body).val();
                    var script = dialogWindow.scriptInput.getValue();
                    self.POST("check_if_decorator_name_is_unique",JSON.stringify({"decorator_name": name}),function(err,res){
                      if(res.is_unique) {
                        self.POST("add_decorator", JSON.stringify({
                          name: name,
                          script: script
                        }), function () {
                          dialogWindow.close();
                          self.updateDecorators();
                        });
                      }
                      else{
                        $(".new-job-decorator-name-already-exists",dialogWindow.$body).show().delay(3000).fadeOut();
                      }
                    });
                  }
                }
              },
              onOpen: function () {
                var dialogWindow = this;
                dialogWindow.scriptNode=$("#new-decorator-script-ace",dialogWindow.$body);
                require(["ace/ace", "ace/ext/language_tools"],function(ace){
                  dialogWindow.scriptInput=ace.edit(dialogWindow.scriptNode.get(0));
                  dialogWindow.scriptInput.setTheme("ace/theme/textmate");
                  dialogWindow.scriptInput.getSession().setMode("ace/mode/text");
                });
                $(".dropdown-menu", dialogWindow.$body).render(rangeNames);
                $(".range-option", dialogWindow.$body).click(function () {
                  var oldScript = dialogWindow.scriptInput.getValue();
                  dialogWindow.scriptInput.setValue(oldScript + "return valueDict['" + $(this).text() + "']\n",1);
                });
              }
            });
          });
        },
        updateDecorators: function () {
            var self = this;
            self.setLoading(true);
            self.GET("get_decorators",function (err,res) {
                var directives = {
                    "decorators": {
                        text: function () {
                            return this.name;
                        },
                        style: function() {
                            return "display: inline"
                        }
                    },
                    "decorator-icons":{
                        style: function(){
                            return "display: inline"
                        }
                    }
                };
                $(".decorator-table-body", self.designerNode).render(res.data, directives);
                self.setLoading(false);
            });
        },
        removeDecorator: function (index) {
            var self = this;
            self.POST("remove_decorator", JSON.stringify({
                index: index
            }),function () {
                self.updateDecorators();
            });
        },

        editDecorator: function (index) {
          var self = this;
          var tasks=[];
          tasks.push(self.GET.bind(self,"get_decorator", JSON.stringify({
            index: index
          })));
          tasks.push(self.GET.bind(self,"get_parameter_range_names"));
          async.parallel(tasks,function (err,ret) {
            var res=ret[0];
            var rangeNames=ret[1];
            self.dialog({
              title: "Edit Decorator",
              body: newDesignerDecoratorTemplate,
              buttons: {
                cancel: {
                  label: "Cancel"
                },
                okay: {
                  label: "Okay",
                  class: "btn btn-primary",
                  callback: function(){
                    var dialogWindow=this;
                    var decoratorName = $(".new-decorator-name", dialogWindow.$body);
                    if (dialogWindow.scriptInput.getValue() != res[0].script) {
                      self.POST("modify_decorator", JSON.stringify({
                        name: res[0].name,
                        key_string: ["script"],
                        value_string: [dialogWindow.scriptInput.getValue()]
                      }),function () {
                        if (decoratorName.val() != res[0].name) {
                          self.POST("check_if_decorator_name_is_unique",JSON.stringify({"decorator_name": decoratorName.val()}),function(err,ret) {
                            if (ret.is_unique) {
                              self.POST("modify_decorator", JSON.stringify({
                                name: res[0].name,
                                key_string: ["name"],
                                value_string: [decoratorName.val()]
                              }), function () {
                                dialogWindow.close();
                                self.updateDecorators();
                              })
                            }
                            else{
                              $(".new-job-decorator-name-already-exists",dialogWindow.$body).show().delay(3000).fadeOut();
                            }
                          });
                        } else {
                          dialogWindow.close();
                          self.updateDecorators();
                        }
                      })
                    } else {
                      if (decoratorName.val != res[0].name) {
                        self.POST("check_if_decorator_name_is_unique",JSON.stringify({"decorator_name": decoratorName.val()}),function(err,ret) {
                          if (ret.is_unique) {
                            self.POST("modify_decorator", JSON.stringify({
                              name: res[0].name,
                              key_string: ["name"],
                              value_string: [decoratorName.val()]
                            }), function () {
                              dialogWindow.close();
                              self.updateDecorators();
                            });
                          }
                          else{
                            $(".new-job-decorator-name-already-exists",dialogWindow.$footer).show().delay(3000).fadeOut();
                          }
                        });
                      } else {
                        dialogWindow.close();
                      }
                    }
                  }
                }
              },
              onOpen: function () {
                var dialogWindow = this;
                var decoratorName = $(".new-decorator-name", dialogWindow.$body);
                dialogWindow.scriptNode=$("#new-decorator-script-ace",dialogWindow.$body);
                require(["ace/ace", "ace/ext/language_tools"],function(ace){
                  dialogWindow.scriptInput=ace.edit(dialogWindow.scriptNode.get(0));
                  dialogWindow.scriptInput.setTheme("ace/theme/textmate");
                  dialogWindow.scriptInput.getSession().setMode("ace/mode/text");
                  dialogWindow.scriptInput.setValue(res[0].script,1);
                });
                decoratorName.val(res[0].name);
                $(".dropdown-menu", dialogWindow.$body).render(rangeNames[0]);
                $(".range-option", dialogWindow.$body).click(function () {
                  var oldScript = dialogWindow.scriptInput.getValue();
                  dialogWindow.scriptInput.setValue(oldScript + "return valueDict['" + $(this).text() + "']\n",1);
                });
              }
            })
          });
        },
        submit: function(){
          var self=this;
          self.POST("set_command",JSON.stringify({command_string: self.commands.val()}),function(){self.GET("get_command_lines",function(err,res){
            var commandText="";
            for (i=0; i<res.commandlines.length; i++){
              commandText+=res.commandlines[i]+"\n";
            }
            self.spawnInstance("jobmanagement","JobSubmission",{commands: commandText});
          })
        });
            },
        updatePreview: function(setCommand){
          var self=this;
          if (setCommand==true) {
            var setCommand = self.POST("set_command", JSON.stringify({
              command_string: self.commands.val()
            }), function () {
              self.GET("get_command_lines", function (err, res) {
                var previewText = "";
                for (i = 0; i < res.commandlines.length - 1; i++) {
                  previewText += res.commandlines[i] + "\n";
                }
                previewText += res.commandlines[res.commandlines.length - 1];
                self.preview.setValue(previewText, 1);
              });
            });
          }
          else{
            self.GET("get_command_lines", function (err, res) {
              var previewText = "";
              for (i = 0; i < res.commandlines.length - 1; i++) {
                previewText += res.commandlines[i] + "\n";
              }
              previewText += res.commandlines[res.commandlines.length - 1];
              self.preview.setValue(previewText, 1);
            });
          }
        },
        saveDesignerJobs: function(){
          var self=this;
          self.POST("set_command",JSON.stringify({
            command_string: self.commands.val()
          }),function() {
            var args = {
              "path": this.path,
              "foldermode": false,
              callback: function (path) {
                self.POST("save_designer_jobs", JSON.stringify({path: path}));
              }
            };
            self.spawnInstance("file", "FileSelector", args);
          })
        },
        loadDesignerJobs: function(){
          var self=this;
          var args={
            "path":this.path,
            "foldermode":false,
            callback: function(path){
              self.POST("load_designer_jobs",JSON.stringify({path: path}),function(){
                self.GET("get_command",function(err,res){
                  self.commands.val(res.command);
                  self.updateDecorators();
                  self.updateOptions(false);
                  self.updateRanges(false);
                  self.updatePreview(false);
                });
              });
            }
          };
          self.spawnInstance("file","FileSelector",args);
        }
    },{
      iconClass: "glyphicon glyphicon-wrench",
      label: "JobDesigner",
      name: "JobDesigner",
      menuPosition: 42
  });
    return JobDesignerView;
});
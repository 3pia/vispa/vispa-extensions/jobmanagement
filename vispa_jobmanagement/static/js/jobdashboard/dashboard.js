/*global vispa */
/*globals vispa*/
define([ "jquery",
  "vispa/views/main",
  "async",
  "text!../../html/jobdashboard.html",
  "text!../../html/job_actions_popover.html",
  "text!../../html/show_log_dialog.html",
  "css!../../css/jobdashboard",
  "css!../../css/jobextension_nodes" ], function($, MainView, async,dashboardTemplate,popoverTemplate,logDialogTemplate) {
  var JobDashboardView = MainView._extend({
    init: function init(args) {
      var self=this;
      init._super.apply(this,arguments);
    },
    render: function (node) {
      var self=this;
      self.renderContent(node);
    },

    onAfterClose: function() {
      var self = this;
      window.clearTimeout(self.polling);
    },

    renderContent: function(node) {
      var self = this;
      self.setLoading(true);
      self.jobOptions = {
        n_jobs: 25,
        async: true,
        reverse: true,
        ordering: "submissionTime"
      };
      self.pageNumber = 1;
      self.pages = 1;
      self.selectedUUIDs = [];
      self.pollingInterval = 10000; // in milli seconds
      self.pollingLocked = true;
      self.updateRunning = false;
      self.isPopoverVisible = false;
      self.incrementInterval = null;
      /* Automatic update of job dashboard:
       * Each n second the function updateJobList() will be called. The function
       * is protected agains calling itself again before it is finished using
       * the member variable "updateRunning".
       * Other functions such as "refreshJobs" or "removeJobs" can lock the
       * update process by setting "self.pollingLocked = true". Do not forget
       * to set it back to false after the method has finished.
       * */

      self.polling = window.setInterval(function() {
        self.updateJobList();
      }, self.pollingInterval);


      self.dashboardNode = $(dashboardTemplate).appendTo(node);
      //save DOM elements to variables
      self.statusUpIcon = $(".status-up", self.dasboardNode);
      self.statusDownIcon = $(".status-down", self.dashboardNode);
      self.statusUnselectedIcon = $(".status-unselected", self.dashboardNode);
      self.creationTimeUpIcon = $(".creation-time-up", self.dashboardNode);
      self.creationTimeDownIcon = $(".creation-time-down", self.dashboardNode);
      self.creationTimeUnselectedIcon = $(".creation-time-unselected", self.dashbardNode);
      self.finishedTimeUpIcon = $(".finished-time-up", self.dashboardNode);
      self.finishedTimeDownIcon = $(".finished-time-down", self.dashboardNode);
      self.finishedTimeUnselectedIcon = $(".finished-time-unselected", self.dashboardNode);
      self.selectAllCheckbox = $(".job-checkbox-all", self.dashboardNode);
      self.NJobsNode = $(".select-n-jobs", self.dashboardNode);
      self.jobStatusNode = $(".select-job-status", self.dashboardNode);
      self.filterByCommandButton = $(".command-button", self.dashboardNode);
      self.filterByCommandInput = $(".filter-command", self.dashboardNode);
      self.dashboardTable = $(".dashboard-table", self.dashboardNode);
      self.popoverContainer = $(".popover-container", self.dashboardNode);
      self.popoverTemplate = popoverTemplate;
      self.popoverDummy = $(".job-actions-popover-dummy", self.dashboardNode);
      self.popoverDummy.popover({
        template: self.popoverTemplate,
        html: "true",
        placement: "auto right"
      });
      self.popoverContainer.on("click", ".popover-open-log", function() {
        self.showJobLogOutput([ self.popoverDummy.uuid ]);
      });
      self.popoverContainer.on("click", ".popover-open-output", function() {
        self.showJobOutput([ self.popoverDummy.uuid ]);
      });
      self.popoverContainer.on("click", ".popover-open-error", function() {
        self.showJobErrorOutput([ self.popoverDummy.uuid ]);
      });
      self.popoverContainer.on("click", ".popover-remove-job", function() {
        self.removeJobs([ self.popoverDummy.uuid ]);
      });
      self.popoverContainer.on("click", ".popover-stop-job", function() {
        self.stopSelectedJobs([ self.popoverDummy.uuid ]);
      });
      self.popoverContainer.on("click", ".popover-restart-job", function() {
        self.restartSelectedJobs([ self.popoverDummy.uuid ]);
        self.setLoading(true);
      });
      //handle click event on cog wheel
      self.dashboardTable.on("click", ".open-job-actions", function(e) {
        if (self.isPopoverVisible == false) {
          var popoverPosition = self.calculatePopoverPosition(e);
          self.popoverDummy.uuid = $(this).closest("tr").attr("uuid");
          self.popoverDummy.css("top", popoverPosition.y + "px");
          self.popoverDummy.css("left", popoverPosition.x + "px");
          self.popoverDummy.popover("show");
          e.stopPropagation();
          self.isPopoverVisible = true;
        }
      });
      $(".vispa-body").on("click", function() {
        self.popoverDummy.popover("hide");
        self.isPopoverVisible = false;
      });
      //handle click event on table row
      self.dashboardNode.on("click", ".dashboard-td", function(event) {
        var td = $(this);
        self.tableSelection(td, event.ctrlKey, event.shiftKey);
      });
      //handle click event on checkbox
      self.dashboardNode.on("click", ".job-checkbox", function(event) {
        event.stopPropagation();
        var tablerow = $(this).closest("tr");
        var tablerowUUID = tablerow.attr("uuid");
        if ($(this).prop("checked") === true) {
          self.lastSelectedRow = tablerow;
          if (self.selectedUUIDs.indexOf(tablerowUUID == -1)) {
            self.selectedUUIDs[self.selectedUUIDs.length] = tablerowUUID;
          } else {
            self.selectedUUIDs.splice(self.selectedUUIDs.indexOf(tablerowUUID), 1);
          }
        }
        self.setJobCheckboxAll();
      });
      //handle right click event
      self.dashboardNode.on("contextmenu",".dashboard-td",function(event){
        event.preventDefault();
        var popoverPosition = self.calculatePopoverPosition(event);
        self.popoverDummy.uuid = $(this).closest("tr").attr("uuid");
        self.popoverDummy.css("top", popoverPosition.y + "px");
        self.popoverDummy.css("left", popoverPosition.x + "px");
        self.popoverDummy.popover("show");
        self.isPopoverVisible = true;
      });

      self.GET("get_available_batchmanager", function(err, res) {
        var availBM = res;

        // render batch manager selector
        self.BMselectionNode = $(".select-batchmanager", self.dashboardNode);
        self.BMselectionNode.render(availBM);
        if (availBM[0].length >= 2 && availBM[0].name == "LocalBatchManager") {
          self.BMselectionNode.val(availBM[1].name);
          self.BMselectionNode.selectpicker("refresh");
        } else {
          self.BMselectionNode.val(availBM[0].name);
          self.BMselectionNode.selectpicker("refresh");
        }
        self.jobOptions.batchmanager = $(".select-batchmanager option:selected", self.dashboardNode).val();
        self.localOutputPath=self.prefs.get("localOutputFolder");
        self.condorOutputPath=self.prefs.get("condorOutputFolder");
        if (self.BMselectionNode.val() == "CondorBatchManager") {
          var foldername = self.condorOutputPath;
        }
        if (self.BMselectionNode.val() == "LocalBatchManager") {
          var foldername = self.localOutputPath;
        }

        self.BMselectionNode.change(function() {
          self.setLoading(true);
          self.renderJobsOverview();
          self.selectedUUIDs = [];
          self.selectAllCheckbox.prop("checked", false);
          self.jobOptions.batchmanager = $(this).val();

          var foldername;
          switch (self.BMselectionNode.val()) {
          case "CondorBatchManager":
            foldername = self.condorOutputPath;
            break;
          case "LocalBatchManager":
          default:
            foldername = self.localOutputPath;
            break;
          }

          self.POST("set_batch_manager_output_folder", {
            foldername: foldername,
            batchmanager: self.jobOptions.batchmanager
          }, function() {
            self.resetPageCounters();
            self.setPages();
            self.jobOptions.only_modified = false;
            self.pollingLocked = true;
            self.fetchJobData(self.processJobData.bind(self));
          });
        });

        self.POST("set_batch_manager_output_folder", {
          foldername: foldername,
          batchmanager: self.jobOptions.batchmanager
        }, function() {
          self.pollingLocked = true;
          self.fetchJobData(self.processJobData.bind(self));
          }
        );

        // add functionality to select buttons
        self.NJobsNode.change(function(event) {
          self.jobOptions.n_jobs = parseInt($("option:selected", event.target)[0].value);
          self.setLoading(true);
          self.resetPageCounters();
          self.setPages();
          self.refreshJobList();
        });

        self.jobStatusNode.change(function(event) {
          self.jobOptions.status = $("option:selected", event.target)[0].value;
          self.setLoading(true);
          self.resetPageCounters();
          self.setPages();
          self.refreshJobList();
        });

        //enable choice of sorting criteria by clicking on table headers
        $(".th-status", self.dashboardNode).click(function() {
          if (self.jobOptions.ordering != "status") {
            self.jobOptions.ordering = "status";
            self.jobOptions.reverse = false;
            self.statusUnselectedIcon.hide();
            self.finishedTimeUnselectedIcon.show();
            self.creationTimeUnselectedIcon.show();
            self.statusUpIcon.show();
            self.creationTimeUpIcon.hide();
            self.creationTimeDownIcon.hide();
            self.finishedTimeUpIcon.hide();
            self.finishedTimeDownIcon.hide();
            self.resetPageCounters();
            self.refreshJobList();
          } else {
            self.statusUpIcon.toggle();
            self.statusDownIcon.toggle();
            self.jobOptions.reverse = !self.jobOptions.reverse;
            self.resetPageCounters();
            self.refreshJobList();
          }
        });
        $(".th-finished-time", self.dashboardNode).click(function() {
          if (self.jobOptions.ordering != "finishedTime") {
            self.jobOptions.ordering = "finishedTime";
            self.jobOptions.reverse = false;
            self.statusUnselectedIcon.show();
            self.finishedTimeUnselectedIcon.hide();
            self.creationTimeUnselectedIcon.show();
            self.statusUpIcon.hide();
            self.statusDownIcon.hide();
            self.creationTimeUpIcon.hide();
            self.creationTimeDownIcon.hide();
            self.finishedTimeUpIcon.show();
            self.resetPageCounters();
            self.refreshJobList();
          } else {
            self.finishedTimeUpIcon.toggle();
            self.finishedTimeDownIcon.toggle();
            self.jobOptions.reverse = !self.jobOptions.reverse;
            self.resetPageCounters();
            self.refreshJobList();
          }
        });
        $(".th-creation-time", self.dashboardNode).click(function() {
          if (self.jobOptions.ordering != "submissionTime") {
            self.jobOptions.ordering = "submissionTime";
            self.jobOptions.reverse = false;
            self.statusUnselectedIcon.show();
            self.finishedTimeUnselectedIcon.show();
            self.creationTimeUnselectedIcon.hide();
            self.statusUpIcon.hide();
            self.statusDownIcon.hide();
            self.creationTimeUpIcon.show();
            self.finishedTimeDownIcon.hide();
            self.finishedTimeUpIcon.hide();
            self.resetPageCounters();
            self.refreshJobList();
          } else {
            self.creationTimeUpIcon.toggle();
            self.creationTimeDownIcon.toggle();
            self.jobOptions.reverse = !self.jobOptions.reverse;
            self.resetPageCounters();
            self.refreshJobList();
          }
        });

        //enable filtering by command
        self.jobOptions.cmd_pattern = "";
        self.filterByCommandButton.click(function() {
          self.jobOptions.cmd_pattern = self.filterByCommandInput.val();
          self.resetPageCounters();
          $(this).toggleClass("btn-default", false);
          $(this).toggleClass("btn-primary", true);
          self.refreshJobList();
        });
        self.filterByCommandInput.keypress(function(e) {
          if (e.which == 13) {
            self.jobOptions.cmd_pattern = self.filterByCommandInput.val();
            self.filterByCommandButton.toggleClass("btn-default", false);
            self.filterByCommandButton.toggleClass("btn-primary", true);
            self.refreshJobList();
          }
        });
        self.filterByCommandInput.on("input", function() {
          self.filterByCommandButton.toggleClass("btn-default", true);
          self.filterByCommandButton.toggleClass("btn-primary", false);
        });

        $(".selectpicker", self.dashboardNode).selectpicker();
        $(".page-up", self.dashboardNode).click(function() {
          if (self.pageNumber < self.pages) {
            self.pageNumber += 1;
            $(".page-number", self.dashboardNode).text(String(self.pageNumber));
            self.jobOptions.n_jobs_min = (self.pageNumber - 1) * self.NJobsNode.val();
            self.refreshJobList();
          }
        });
        $(".page-down", self.dashboardNode).click(function() {
          if (self.pageNumber > 1) {
            self.pageNumber -= 1;
            $(".page-number", self.dashboardNode).text(String(self.pageNumber));
            self.jobOptions.n_jobs_min = (self.pageNumber - 1) * self.NJobsNode.val();
            self.refreshJobList();
          }
        });
        $(".first-page", self.dashboardNode).click(function() {
          if (self.pageNumber != 1) {
            self.pageNumber = 1;
            $(".page-number", self.dashboardNode).text(String(self.pageNumber));
            self.jobOptions.n_jobs_min = 0;
            self.refreshJobList();
          }
        });
        $(".last-page", self.dashboardNode).click(function() {
          if (self.pageNumber != self.pages) {
            self.pageNumber = self.pages;
            $(".page-number", self.dashboardNode).text(String(self.pageNumber));
            self.jobOptions.n_jobs_min = (self.pageNumber - 1) * self.NJobsNode.val();
            self.refreshJobList();
          }
        });
        self.incrementInterval = setInterval(function() {
          var tdlist = $("td:contains('Running')", self.dashboardNode).parent().find("td[data-bind='runtime']");
          tdlist.each(function(i, object) {
            object.textContent = self.getFormattedTimeDifference(parseInt(object.value) + 1);
            object.value = parseInt(object.value) + 1;
          });
        }, 1000);

      });

      return this;
    },

    fetchJobData: function(callback) {
      var self = this;
      self.logger.debug("called fetch job data");
      var checkDone = function(err, res, a) {
        if (res.success === true) {
          self.jobOptions.async = false;
          callback(res.data);
          // self.processJobData(res.data);
        } else {

          var info = res.info;
          window.setTimeout(self.GET("get_job_data", JSON.stringify(self.jobOptions), checkDone),
              1000);
        }
      };
      self.jobOptions.async = true;
      self.GET("get_job_data", JSON.stringify(self.jobOptions), checkDone);
    },

    processJobData: function(res) {
      var self = this;
      self.setPages();
      self.currentJobList = res;
      self.renderJobList(self.dashboardNode.find("tbody"), res);
      self.selectAllCheckbox.click(function() {
        $(".job-checkbox", self.dashboardNode).prop("checked", this.checked);
        if (self.selectAllCheckbox.prop("checked") === true) {
          for (var k = 1; (k <= self.NJobsNode.val()) && (k <= self.numberOfJobs); k++) {
            var table = $(".dashboard-table", self.dashboardNode);
            var uuid = $(table.prop("rows")[k]).attr("uuid");
            if ($.inArray(self.selectedUUIDs, uuid) == -1) {
              self.selectedUUIDs[self.selectedUUIDs.length] = uuid;
            }
          }
        } else {
          self.selectedUUIDs = [];
        }
      });
      document.onkeydown = function(event) {
        if (event.ctrlKey === true || event.shiftKey === true) {
          $(".dashboard-td", self.dashboardNode).disableSelection();
        }
      };
      document.onkeyup = function() {
        $(".dashboard-td", self.dashboardNode).enableSelection();
      };
      self.pollingLocked = false;
      self.logger.debug("processJobData: unlocking polling");
      self.setLoading(false);
    },

    // only get changes
    updateJobList: function() {
      var self = this;
      if (self.pollingLocked === true || self.updateRunning === true) {
        self.logger.debug("updateJobList(): polling is locked or running.");
        return 0;
      }
      self.updateRunning = true;
      var callback = function(modifiedJobs) {
        if (modifiedJobs.length > 0) {
          self.setPages();
          if ((self.jobOptions.n_jobs == modifiedJobs.length) || (modifiedJobs.length > self.currentJobList.length)) {
            self.currentJobList = modifiedJobs;
          } else {
            $.each(self.currentJobList, function(i, job) {
              job = $.grep(modifiedJobs, function(element) {
                return (element.id == job.id);
              });
              if (job.length) {
                self.currentJobList[i] = job[0];
              }
              job = $.grep(modifiedJobs, function(element) {
                return (element.id == job.id);
              }, true);
              if (job.length) {
                self.currentJobList.push.apply(job, self.currentJobList);
              }
            });
          }
          if (self.pollingLocked === true) { // check if no lock has been activated in the meanwhile
            self.updateRunning = false;
            self.setLoading(false);
            self.logger.debug("updateJobList: polling has been locked in the meanwhile");
            return 0;
          }
          self.renderJobList(self.dashboardNode.find("tbody"), self.currentJobList);
        }
        $("tbody [type='checkbox']", self.dashboardNode).each(function(index, element) {
          if ($.inArray($(element).closest("tr").attr("uuid"), self.selectedUUIDs) != -1) {
            $(element).prop("checked", true);
          } else {
            $(element).prop("checked", false);
          }
        });
        self.logger.debug("update finished, setting running to false");
        self.updateRunning = false;
        self.jobOptions.only_modified = false;
        self.pollingLocked = false;
        self.logger.debug("update finished, remove polling lock");
        self.setLoading(false);
      };
      self.jobOptions.only_modified = true;
      self.logger.debug("get modified job");
      self.fetchJobData(callback);
    },

    // get complete job list
    refreshJobList: function() {
      var self = this;
      self.logger.debug("refresh job list: polling locked");
      self.pollingLocked = true;
      if (self.updateRunning === true) {
        self.logger.debug("update currently running, starting refreshJobList anyway");
      }
      self.setLoading(true);
      self.jobOptions.only_modified = false;
      var callback = function(data) {
        self.currentJobList = data;
        self.renderJobList(self.dashboardNode.find("tbody"), data);
        $("tbody [type='checkbox']", self.dashboardNode).each(function(index, element) {
          if ($.inArray($(element).closest("tr").attr("uuid"), self.selectedUUIDs) != -1) {
            $(element).prop("checked", true);
          } else {
            $(element).prop("checked", false);
          }
        });
        self.setPages();
        self.pollingLocked = false;
        self.logger.debug("refresh job list ready, unlocking polling");
        self.setLoading(false);
      };
      self.fetchJobData(callback);
    },

    //helper that converts time stamp into days, hours etc.
    getFormattedTimeDifference: function(timestamp) {
      var days = Math.floor(timestamp / (3600 * 24));
      var hours = Math.floor((timestamp - days * 3600 * 24) / 3600);
      var minutes = Math.floor((timestamp - days * 3600 * 24 - hours * 3600) / 60);
      var seconds = Math.floor((timestamp - days * 3600 * 24 - hours * 3600 - minutes * 60));
      if (days > 0) {
        return days + " days " + hours + "h " + minutes + "m " + seconds + "s";
      }
      if (hours > 0) {
        return hours + "h " + minutes + "m " + seconds + "s";
      }
      if (minutes > 0) {
        return minutes + "m " + seconds + "s";
      }
      return seconds + "s";
    },

    renderJobList: function(node, data) {
      var self = this;
      self.renderJobsOverview();
      var getFormatedDateString = function(timestamp) {
        var time = new Date(timestamp * 1000);
        return time.toLocaleString();
      };
      var directives = {
        "file-size-notification": {
          style: function() {
            if ((this.errorfilesize > 1e6) || (this.outputfilesize > 1e6)) {
              return "cursor: default";
            }
          },

          title: function() {
            if (this.errorfilesize > 1e6 && this.outputfilesize > 1e6) {
              return "Warning: Output and Error file too large\n Error file size = " + this.errorfilesize / 1000
                  + " kB\n Output file size = " + this.outputfilesize / 1000 + " kB";
            }
            if (this.errorfilesize > 1e6) {
              return "Warning: Error file too large\n Error file size = " + this.errorfilesize / 1000 + " kB";
            }
            if (this.outputfilesize > 1e6) {
              return "Warning: Output file too large\n Output file size = " + this.outputfilesize / 1000 + " kB";
            }
          }
        },

        "tr-class-name": {
          "class": function() {
            if (this.status == "Finished Normally")
              return "success";
            else if (this.status == "Finished Abnormally")
              return "warning";
          },
          uuid: function() {
            return this.id;
          }
        },
        "runtime": {
          text: function() {
            if (this.runTime && this.runTime != "None") {
              return self.getFormattedTimeDifference(this.runTime);
            } else {
              if (this.finishedTime > this.submissionTime) {
                return self.getFormattedTimeDifference(this.finishedTime - this.submissionTime);
              } else {
                if (this.status == "Running") {
                  if (this.executionTime && this.executionTime != "None") {
                    return self.getFormattedTimeDifference(Date.now() / 1000 - this.executionTime);
                  } else {
                    return self.getFormattedTimeDifference(Date.now() / 1000 - this.submissionTime);
                  }
                } else {
                  if (this.status == "Finished Normally" || this.status == "Finished Abnormally") {
                    return "0s";
                  }
                  return "---";
                }
              }
            }
          },
          value: function() {
            if (this.runTime && this.runTime != "None") {
              return this.runTime;
            } else {
              if (this.finishedTime > this.submissionTime) {
                return this.finishedTime - this.submissionTime;
              } else {
                if (this.status == "Running" || this.status == "Pending") {
                  if (this.executionTime && this.executionTime != "None") {
                    return Date.now() / 1000 - this.executionTime;
                  } else {
                    return Date.now() / 1000 - this.submissionTime;
                  }
                } else {
                  return 0;
                }
              }
            }
          }
        },
        "submissiontime": {
          text: function() {
            return getFormatedDateString(this.submissionTime);
          }
        },
        "finishedtime": {
          text: function() {
            if ((this.status != "Running") && (this.status != "Created") && (this.status != "Pending")) {
              return getFormatedDateString(this.finishedTime);
            } else {
              return "Not Finished";
            }
          }
        },
        "show-error-option": {
          "class": function() {
            if (this.errorfilesize === 0) {
              return "disabled";
            } else {
              return "show-error-option";
            }
          }
        },
        "show-output-option": {
          "class": function() {
            if (this.outputfilesize === 0) {
              return "disabled";
            } else {
              return "show-output-option";
            }
          }
        },
        "stop-job-option": {
          "class": function() {
            if ((this.status == "Finished Normally") || (this.status == "Finished Abnormally")
                || (this.status == "Suspended")) {
              return "disabled";
            } else {
              return "stop-job-option";
            }
          }
        }
      };
      node.render(data, directives);
      self.setLoading(false);
    },

    restartSelectedJobs: function(uuids) {
      var self = this;
      if (uuids.length > 0) {
        self.POST("restart_jobs", JSON.stringify({
          jobids: uuids,
          manager: self.jobOptions.batchmanager
        }), self.refreshJobList.bind(self));
      }
    },

    stopSelectedJobs: function(uuids) {
      var self = this;
      if (uuids.length > 0) {
        self.POST("stop_jobs", JSON.stringify({
          jobids: uuids,
          manager: self.jobOptions.batchmanager
        }), self.refreshJobList.bind(self));
      }
    },

    removeSelectedJobs: function() {
      var self = this;
      self.pollingLocked = true;
      var uuids = self.getSelectedJobs();
      if (uuids.length > 0) {
        self.confirm("Are you sure you want to remove "+String(uuids.length)+" jobs?",function(confirmed){
          if (confirmed) {
            self.removeJobs(uuids);
            self.setLoading(true);
          }
        });
      }
    },

    //removes all jobs with given ids

    asyncRequest: function(ajaxName, options, callback) {
      var self = this;
      self.pollingLocked = true;
      self.logger.debug("asyncRequest: locking polling");
      var checkDone = function(err, res, a) {
        if (res.success === true) {
          self.logger.debug("async request done");
          callback(res.data);
        } else {
          self.logger.debug("async not yet ready");
          var info = res.info;
          options = {manager: self.jobOptions.batchmanager};
          window.setTimeout(self.GET("is_async_ready",
              JSON.stringify(options), checkDone), 1000);
        }
      };
      options.async = true;
      options.manager = self.jobOptions.batchmanager;
      self.POST(ajaxName, JSON.stringify(options), checkDone);
    },

    removeJobs: function(uuids) {
      var self = this;
      self.setLoading(true);
      if (uuids.length > 0) {
        self.asyncRequest("remove_jobs", {jobids: uuids}, function() {
          $(".job-checkbox", self.dashboardNode).prop("checked", false);
          self.selectAllCheckbox.prop("checked", false);
          self.selectedUUIDs = [];
          self.refreshJobList();
          self.setPages();

          self.setLoading(false);
        });
      }
    },

    getSelectedJobs: function() {
      var self = this;
      var checkboxes = $("tbody [type='checkbox']:checked:enabled", self.dashboardNode);
      var uuids = [];
      checkboxes.each(function(index, element) {
        uuids.push($(element).closest("tr").attr("uuid"));
      });
      return uuids;
    },

    //opens the job's output file in a dialog window
    showJobOutput: function(uuids) {
      var self = this;
      if (uuids.length == 1) {
        var currentJob;
        var len = self.currentJobList.length;
        for (var i = 0; i < len; i++) {
          if (self.currentJobList[i].id == uuids[0]) {
            currentJob = self.currentJobList[i];
          }
        }
        if (currentJob.outputfilesize > 0) {
          self._showOutput("get_job_output", {
            jobid: uuids[0],
            manager: self.jobOptions.batchmanager
          }, "Job Output");
        } else {
          alert("Output file does not exist");
        }
      }
      if (uuids.length > 1) {
        alert("Please select only one job.");
      }
    },

    //shows the job's error file in a dialog window
    showJobErrorOutput: function(uuids) {
      var self = this;
      if (uuids.length == 1) {
        var currentJob;
        var len = self.currentJobList.length;
        for (var i = 0; i < len; i++) {
          if (self.currentJobList[i].id == uuids[0]) {
            currentJob = self.currentJobList[i];
          }
        }
        if (currentJob.errorfilesize > 0) {
          self._showOutput("get_job_error_output", {
            jobid: uuids[0],
            manager: self.jobOptions.batchmanager
          }, "Job Error Output");
        } else {
          alert("Error file does not exist");
        }
      }
      if (uuids.length > 1) {
        alert("Please select only one job.");
      }
    },

    //shows a job's log file in a dialog window
    showJobLogOutput: function(uuids) {
      var self = this;
      if (uuids.length == 1) {
        self._showOutput("get_job_log_output", {
          jobid: uuids[0],
          manager: self.jobOptions.batchmanager
        }, "Job Log Output");
      }
      if (uuids.length > 1) {
        alert("Please select only one job.");
      }
    },

    _showOutput: function(url, data, header) {
      var self = this;
      self.GET(url,data, function(err, res) {
        self.dialog({
          title: header,
          body: logDialogTemplate,
          onOpen: function() {
            var dialogWindow = this;
            $(".jobdashboard-dialog-text", dialogWindow.$body).val(res.content);
          }
        });
      });
    },


    //handles selection of jobs by clicking on table
    tableSelection: function(td, control, shift) {
      var self = this;
      var table;
      var tr = td.closest("tr");
      var checkbox = $(".job-checkbox", tr);
      if (checkbox.prop("checked") === false) {
        if (control === false && shift === false) {
          $(".job-checkbox", self.dashboardNode).prop("checked", false);
          self.selectAllCheckbox.prop("checked", false);
          self.selectedUUIDs = [ tr.attr("uuid") ];
          checkbox.prop("checked", true);
        }
        if (shift === true && typeof (self.lastSelectedRow) != "undefined") {
          table = tr.closest("table");
          if (tr.prop("rowIndex") > self.lastSelectedRow.prop("rowIndex")) {
            for (var i = self.lastSelectedRow.prop("rowIndex") + 1; i <= tr.prop("rowIndex"); i++) {
              $(".job-checkbox", table.prop("rows")[i]).prop("checked", true);
              if (self.selectedUUIDs.indexOf($(table.prop("rows")[i]).attr("uuid")) == -1) {
                self.selectedUUIDs[self.selectedUUIDs.length] = $(table.prop("rows")[i]).attr("uuid");
              }
            }
          }
          if (self.lastSelectedRow.prop("rowIndex") > tr.prop("rowIndex")) {
            for (var j = tr.prop("rowIndex"); j <= self.lastSelectedRow.prop("rowIndex") - 1; j++) {
              $(".job-checkbox", table.prop("rows")[j]).prop("checked", true);
              if (self.selectedUUIDs.indexOf($(table.prop("rows")[j]).attr("uuid")) == -1) {
                self.selectedUUIDs[self.selectedUUIDs.length] = $(table.prop("rows")[j]).attr("uuid");
              }
            }
          }
        }
        if (shift === true && typeof (self.lastSelectedRow) == "undefined") {
          $(".job-checkbox", self.dashboardNode).prop("checked", false);
          self.selectAllCheckbox.prop("checked", false);
          self.selectedUUIDs = [ tr.attr("uuid") ];
          checkbox.prop("checked", true);
        }

        if (control === true) {
          checkbox.prop("checked", true);
          if (self.selectedUUIDs.indexOf(tr.attr("uuid")) == -1) {
            self.selectedUUIDs[self.selectedUUIDs.length] = tr.attr("uuid");
          }
        }
        self.lastSelectedRow = tr;
      } else {
        if (control === false && shift === false) {
          $(".job-checkbox", self.dashboardNode).prop("checked", false);
          self.selectAllCheckbox.prop("checked", false);
          self.selectedUUIDs = [];
        } else {
          if (control === true && shift === false) {
            self.selectedUUIDs.splice(self.selectedUUIDs.indexOf(tr.attr("uuid")), 1);
            checkbox.prop("checked", false);
          }
          if (shift === true) {
            table = tr.closest("table");
            if (tr.prop("rowIndex") > self.lastSelectedRow.prop("rowIndex")) {
              for (var o = self.lastSelectedRow.prop("rowIndex"); o <= tr.prop("rowIndex"); o++) {
                $(".job-checkbox", table.prop("rows")[o]).prop("checked", true);
                if ($.inArray(self.selectedUUIDs, $(table.prop("rows")[o]).attr("uuid")) == -1) {
                  self.selectedUUIDs[self.selectedUUIDs.length] = $(table.prop("rows")[o]).attr("uuid");
                }
              }
            }
            if (self.lastSelectedRow.prop("rowIndex") > tr.prop("rowIndex")) {
              for (var p = tr.prop("rowIndex"); p < self.lastSelectedRow.prop("rowIndex") + 1; p++) {
                $(".job-checkbox", table.prop("rows")[p]).prop("checked", true);
                if ($.inArray(self.selectedUUIDs, $(table.prop("rows")[p]).attr("uuid")) == -1) {
                  self.selectedUUIDs[self.selectedUUIDs.length] = $(table.prop("rows")[p]).attr("uuid");
                }
              }
            }
          }
        }
      }
      self.setJobCheckboxAll();
    },

    renderJobsOverview: function() {
      var self = this;
      self.POST("get_number_of_jobs_by_status", {
        manager: self.BMselectionNode.val()
      }, function(err, res) {
        $(".overview-total", self.dashboardNode).text(String(res.total));
        $(".overview-normally", self.dashboardNode).text(String(res.normally));
        $(".overview-abnormally", self.dashboardNode).text(String(res.abnormally));
        $(".overview-running", self.dashboardNode).text(String(res.running));
        $(".overview-pending", self.dashboardNode).text(String(res.pending));
        $(".overview-suspended", self.dashboardNode).text(String(res.suspended));
      });
    },

    //determines the number of pages and sets the display accordingly
    setPages: function() {
      var self = this;
      self.GET("get_number_of_jobs", {
        manager: self.BMselectionNode.val(),
        status: self.jobStatusNode.val(),
        command: self.jobOptions.cmd_pattern
      }, function(err, res) {
        var pages;
        if (self.NJobsNode.val() !== 0) {
          pages = Math.ceil(res.number / self.NJobsNode.val());
        } else {
          pages = 1;
        }
        if (pages === 0) {
          pages = 1;
        }
        if (self.NJobsNode.val() == 0) {
          pages = 1;
        }
        $(".pages", self.dashboardNode).text(String(pages));
        self.pages = pages;
        if(self.pageNumber>self.pages){
          self.pageNumber -= 1;
          $(".page-number", self.dashboardNode).text(String(self.pageNumber));
          self.jobOptions.n_jobs_min = (self.pageNumber - 1) * self.NJobsNode.val();
          self.refreshJobList();
        }
        self.numberOfJobs = res.number;
        self.setJobCheckboxAll();
      });
    },

    //makes the dashboard display the first page of jobs
    resetPageCounters: function() {
      var self = this;
      self.pageNumber = 1;
      $(".page-number", self.dashboardNode).text("1");
      self.jobOptions.n_jobs_min = 0;
    },

    //determines if "select all" checkbox should be checked
    setJobCheckboxAll: function() {
      var self = this;
      var selectedJobNumber = self.getSelectedJobs().length;
      if (((selectedJobNumber == self.numberOfJobs) || (selectedJobNumber == self.jobOptions.n_jobs))
          && (self.numberOfJobs !== 0) && selectedJobNumber !== 0) {
        self.selectAllCheckbox.prop("checked", true);
      } else {
        self.selectAllCheckbox.prop("checked", false);
      }

    },

    //used to determine the position of the dropdown menu
    calculatePopoverPosition: function(clickEvent) {
      var self = this;
      var offset = self.dashboardNode.offset();
      var popoverPosition = {};
      self.popoverDummy.popover({placement:"right"});
      popoverPosition.x = clickEvent.clientX - offset.left;
      popoverPosition.y = clickEvent.clientY - offset.top + 50;
      return popoverPosition;
    }

  },{
      iconClass: "glyphicon glyphicon-list",
      label: "JobDashboard",
      name: "JobDashboard",
      menuPosition: 40,
      menu: {
        refresh: {
          label: "refresh",
          iconClass: "glyphicon glyphicon-refresh",
          callback: function () {
            this.$root.instance.refreshJobList();
          }
        },
        restartJobs: {
          label: "restart selected jobs",
          iconClass: "glyphicon glyphicon-retweet",
          callback: function(){
            this.$root.instance.restartSelectedJobs(this.$root.instance.getSelectedJobs());
          }
        },
        stopJobs: {
          label: "stop selected jobs",
          iconClass: "glyphicon glyphicon-stop",
          callback: function(){
            this.$root.instance.stopSelectedJobs(this.$root.instance.getSelectedJobs());
          }
        },
        removeJobs: {
          label: "remove selected jobs",
          iconClass: "glyphicon glyphicon-trash",
          callback: function(){
            this.$root.instance.removeSelectedJobs(this.$root.instance.getSelectedJobs());
          }
        }
      },
      preferences: {
        items:{
          localOutputFolder: {
            label: "localOutputFolder",
            level: 1,
            type: "string",
            value: "$HOME/.vispa/BatchSystem",
            description: "Output folder for the Local Batch Manager"
          },
          condorOutputFolder: {
            label: "condorOutputFolder",
            level: 1,
            type: "string",
            value: "$HOME/.vispa/BatchSystem",
            description: "Output folder for the Condor Batch Manager"
          }
        }
      }
  });
  return JobDashboardView;
});
/*global vispa */
/*globals vispa*/
define(["jquery",
  "vispa/views/main",
  "require",
  "async",
  "text!../../html/jobsubmission.html",
  "text!../../html/LocalBatchManager_status.html",
  "text!../../html/CondorBatchManager_status.html",
  "css!../../css/jobsubmission",
  "css!../../css/jobextension_nodes"],function($,MainView,require,async,jobsubmissionTemplate,localBatchManagetStatusTemplate,condorBatchManagerStatusTemplate) {
    var JobSubmissionView = MainView._extend({
      init: function init(args) {
        var self=this;
        init._super.apply(this,arguments);
        self.state.setup({
          commands: undefined
        },args);
      },

      render: function (node) {
        var self=this;
        self.renderContent(node);
      },

      renderContent: function (node) {
        var self = this;
        self.setLoading(true);
        self.submissionNode = $(jobsubmissionTemplate).appendTo(node);
        $(".submission-collapse",self.submissionNode).prop("id",self.id+"accordion");
        $(".collapse-trigger-pre-exec",self.submissionNode).attr("data-parent","#"+self.id+"accordion").prop("href","#"+self.id+"collapse-pre-exec");
        $(".collapse-pre-exec",self.submissionNode).prop("id",self.id+"collapse-pre-exec");
        $(".collapse-trigger-commands",self.submissionNode).attr("data-parent","#"+self.id+"accordion").prop("href","#"+self.id+"collapse-commands");
        $(".collapse-commands",self.submissionNode).prop("id",self.id+"collapse-commands");
        $(".collapse-trigger-post-exec",self.submissionNode).attr("data-parent","#"+self.id+"accordion").prop("href","#"+self.id+"collapse-post-exec");
        $(".collapse-post-exec",self.submissionNode).attr("id",self.id+"collapse-post-exec");
        self.GET("get_available_batchmanager",function (err,res) {
          //save DOM elements to variables
          self.submitJobButton = $(".submit-job-button", self.submissionNode);
          self.preExecEditorNode = self.submissionNode.find("#pre-exec-ace");
          self.commandsEditorNode = self.submissionNode.find("#commands-ace");
          self.postExecEditorNode = self.submissionNode.find("#post-exec-ace");
          self.BMSelectionNode = $(".select-batchmanager", self.submissionNode);
          require(["ace/ace", "ace/ext/language_tools"],function(ace){
            self.preExecEditor = ace.edit(self.preExecEditorNode.get(0));
            self.preExecEditor.setTheme("ace/theme/textmate");
            self.preExecEditor.getSession().setMode("ace/mode/text");
            self.commandsEditor = ace.edit(self.commandsEditorNode.get(0));
            self.commandsEditor.setTheme("ace/theme/textmate");
            self.commandsEditor.getSession().setMode("ace/mode/text");
            if(self.state.get("commands")!=null) {
              var commands=self.state.get("commands");
              commands=commands.substring(0,commands.length-1);
              self.commandsEditor.setValue(commands, -1);
            }
            self.postExecEditor = ace.edit(self.postExecEditorNode.get(0));
            self.postExecEditor.setTheme("ace/theme/textmate");
            self.postExecEditor.getSession().setMode("ace/mode/text");
          });
          var directives = {
            "name": {
              title: function () {
                return this.name;
              },
              html: function () {
                return this.description;
              },
              value: function () {
                return this.name;
              }
            }
          };
          self.BMSelectionNode.render(res, directives);
          $(".selectpicker").selectpicker({
            showContent: false
          });
          if (res.length >= 2 && res[0].name == "LocalBatchManager") {
            self.BMSelectionNode.selectpicker("val", res[1].name);
          } else {
            self.BMSelectionNode.selectpicker("val", res[0].name);
          }
          self.batchmanagerName = self.BMSelectionNode.val();
          self.BMinfo = self.renderBatchSystemStatus(
            self.batchmanagerName, $(".batch-manager-info", self.submissionNode));
          self.submitJobButton.click(function () {
            self.submitJobs();
          });
          self.localOutputPath=self.prefs.get("submissionLocalOutputFolder");
          self.condorOutputPath=self.prefs.get("submissionCondorOutputFolder");
          if (self.BMSelectionNode.val() == "CondorBatchManager") {
            self.POST("set_batch_manager_output_folder", {
              foldername: self.condorOutputPath,
              batchmanager: "CondorBatchManager"
            },function(err){self.setLoading(false)});
          }
          else if (self.BMSelectionNode.val() == "LocalBatchManager") {
            self.POST("set_batch_manager_output_folder", {
              foldername: self.localOutputPath,
              batchmanager: "LocalBatchManager"
            },function(err){
              self.setLoading(false)});
          }
          //Set functions for buttons
          $(".save-job-button", self.submissionNode).click(function () {
            self.saveJob();
          });
          $(".load-job-button", self.submissionNode).click(function () {
            self.loadJob();
          });
          $(".load-pre-exec-script", self.submissionNode).click(function () {
            self.openFile(self.preExecEditor);
          });
          $(".load-post-exec-script", self.submissionNode).click(function () {
            self.openFile(self.postExecEditor);
          });
          $(".load-command-script", self.submissionNode).click(function () {
            self.openFile(self.commandsEditor);
          });
          $(".save-command-script", self.submissionNode).click(function () {
            self.saveFile(self.commandsEditor);
          });
          $(".save-pre-exec-script", self.submissionNode).click(function () {
            self.saveFile(self.preExecEditor);
          });
          $(".save-post-exec-script", self.submissionNode).click(function () {
            self.saveFile(self.postExecEditor);
          });
          self.BMSelectionNode.change(function () {
            self.setLoading(true);
            self.batchmanagerName = self.BMSelectionNode.val();
            self.BMinfo = self.renderBatchSystemStatus(self.batchmanagerName,
              $(".batch-manager-info", self.submissionNode));
            if (self.batchmanagerName == "CondorBatchManager") {
              self.POST("set_batch_manager_output_folder", {
                foldername: self.condorOutputPath,
                batchmanager: "CondorBatchManager"
              },function(err){self.setLoading(false)});
            }
            if (self.batchmanagerName == "LocalBatchManager") {
              self.POST("set_batch_manager_output_folder", {
                foldername: self.localOutputPath,
                batchmanager: "LocalBatchManager"
              },function(err){self.setLoading(false)});
            }
          });
        });
        return this;
      },

      renderBatchSystemStatus: function (batchmanagerName, node) {
        var self = this;
        self.GET("get_batchmanager_configuration", {batchmanager: batchmanagerName},function (err, res) {
          var tmpl;
          if (batchmanagerName=="LocalBatchManager"){
            tmpl=localBatchManagetStatusTemplate;
          }else if(batchmanagerName=="CondorBatchManager"){
            tmpl=condorBatchManagerStatusTemplate;
          }else{
            return null;
          }
          if (res.batchmanager == "CondorBatchManager") {
            for (var i = 0; i < res.userpriorities.length; ++i) {
              res.userpriorities[i].userName = res.userpriorities[i].userName.split("@")[0];
            }
          }
          node.empty();
          var newNode = $(tmpl).appendTo(node);
          $(".selectpicker", newNode).selectpicker();
          if (res.batchmanager == "CondorBatchManager") {
            $("#collapsepriority", newNode).on("show.bs.collapse", function () {
              $(".expand-userpriorities", newNode).toggle();
              $(".collapse-userpriorities", newNode).toggle();
            });
            $("#collapsepriority", newNode).on("hide.bs.collapse", function () {
              $(".expand-userpriorities", newNode).toggle();
              $(".collapse-userpriorities", newNode).toggle();
            });
          }
          newNode.render(res);
          return newNode;
        });
      },
        //used when a job is loaded.
      loadBatchSystemStatus: function (batchmanagerName, node, dic) {
        var self = this;
        self.GET("get_batchmanager_configuration", {batchmanager: batchmanagerName},function (err, res) {
          var tmpl;
          if (batchmanagerName=="LocalBatchManager"){
            tmpl=localBatchManagetStatusTemplate;
          }else if(batchmanagerName=="CondorBatchManager"){
            tmpl=condorBatchManagerStatusTemplate;
          }else{
            return null;
          }
          if (res[0].batchmanager == "CondorBatchManager") {
            for (var i = 0; i < res[0].userpriorities.length; ++i) {
              res[0].userpriorities[i].userName = res[0].userpriorities[i].userName.split("@")[0];
            }
          }
          node.empty();
          var newNode = $(tmpl).appendTo(node);
          $(".selectpicker", newNode).selectpicker();
          if (res[0].batchmanager == "CondorBatchManager") {
            $("#collapsepriority", newNode).on("show.bs.collapse", function () {
              $(".expand-userpriorities", newNode).toggle();
              $(".collapse-userpriorities", newNode).toggle();
            });
            $("#collapsepriority", newNode).on("hide.bs.collapse", function () {
              $(".expand-userpriorities", newNode).toggle();
              $(".collapse-userpriorities", newNode).toggle();
            });
          }
          if (dic.batchmanagerName == "CondorBatchManager") {
            $(".select-send-mail", newNode).selectpicker("val", dic.sendEmail);
            $(".queue", newNode).val(dic.queue);
          }
          newNode.render(res[0]);
          return newNode;
        });
      },

      submitJobs: function () {
        var self = this;
        var commandList = self.commandsEditor.getValue().split("\n");
        var preExecutionScript = self.preExecEditor.getValue();
        var postExecutionScript = self.postExecEditor.getValue();
        var requirements;
        var sendEmail;
        var queues;
        var memory;
        var cpus;
        var gpus;
        if (self.batchmanagerName == "CondorBatchManager") {
          sendEmail = $(".select-send-mail", self.BMinfo).val();
          queues = $(".queue", self.BMinfo).val();
          requirements=$(".condor-requirements-input",self.BMinfo).val();
          memory=$(".condor-memory",self.BMinfo).val();
          cpus=$(".condor-cpus",self.BMinfo).val();
          gpus=$(".condor-gpus",self.BMinfo).val();
        } else {
          sendEmail = "never";
          queues = 1;
          requirements=null;
          memory=null;
          cpus=null;
          gpus=null;
        }
        self.POST("submit_jobs", JSON.stringify({
          commands: commandList,
          pre_execution_script: preExecutionScript,
          post_execution_script: postExecutionScript,
          manager: self.batchmanagerName,
          notification: sendEmail,
          queue: queues,
          requirements: requirements,
          memory: memory,
          cpus: cpus,
          gpus: gpus
        }));
        self.submitJobButton.toggleClass("btn-success", true);
        window.setTimeout(function () {
          self.submitJobButton.delay(500).toggleClass("btn-success", false);
        }, 1000);
      },

        openFile: function (target) {
            var self = this;
            var args = {
                "path": this.path,
                callback: function (path) {
                    self.GET("/ajax/fs/getfile", {
                        path: path
                    },function (err,res) {
                        target.setValue(res.content);
                    });
                }
            };
            self.spawnInstance("file","FileSelector", args);
            return this;
        },

        saveFile: function (source) {
            var self = this;
            var args = {
                callback: function (path) {
                    self.POST("/ajax/fs/savefile", {
                        path: path,
                        content: source.getValue(),
                        utf8: true,
                        watch_id: "code"
                    });
                }
            };
            self.spawnInstance("file","FileSelector", args);
            return this;
        },

        saveJob: function () {
          var self = this;
          var obj = {
            preExecutionScript: self.preExecEditor.getValue(),
            postExecutionScript: self.postExecEditor.getValue(),
              commands: self.commandsEditor.getValue(),
            batchmanagerName: self.batchmanagerName};
          if (self.batchmanagerName == "CondorBatchManager") {
            obj.sendEmail = $(".select-send-mail", self.BMinfo).val();
            obj.queue = $(".queue", self.BMinfo).val();
          }
          var args = {
            callback: function (path) {
              self.POST("/ajax/fs/savefile", {
                path: path,
                content: JSON.stringify(obj),
                utf8: true,
                watch_id: "code"
              });
            }
          };
          self.spawnInstance("file","FileSelector", args);
        },

        loadJob: function () {
            var self = this;
            var args = {
                "path": this.path,
                callback: function (path) {
                    self.GET("/ajax/fs/getfile", {
                        path: path
                    },function (err,res) {
                        var dic = JSON.parse(res.content);
                        self.preExecEditor.setValue(dic.preExecutionScript);
                        self.postExecEditor.setValue(dic.postExecutionScript);
                        self.commandsEditor.setValue(dic.commands);
                        self.batchmanagerName = dic.batchmanagerName;
                        if (self.BMSelectionNode.val() != dic.batchmanagerName) {
                            self.BMSelectionNode.selectpicker("val", dic.batchmanagerName);
                            self.loadBatchSystemStatus(dic.batchmanagerName,
                                $(".batch-manager-info", self.submissionNode), dic);
                        } else {
                            if (dic.batchmanagerName == "CondorBatchManager") {
                                $(".select-send-mail", self.BMinfo).selectpicker("val", dic.sendEmail);
                                $(".queue", self.BMinfo).val(dic.queue);
                            }
                        }
                    });
                }
            };
            self.spawnInstance("file","FileSelector", args);
            return this;
        }
    },{
      iconClass: "glyphicon glyphicon-share-alt",
      label: "JobSubmission",
      name: "JobSubmission",
      menuPosition: 41,
      preferences: {
        items:{
          submissionLocalOutputFolder: {
            label: "LocalOutputFolder",
            level: 1,
            type: "string",
            value: "$HOME/.vispa/BatchSystem",
            description: "Output folder for the Local Batch Manager"
          },
          submissionCondorOutputFolder: {
            label: "CondorOutputFolder",
            level: 1,
            type: "string",
            value: "$HOME/.vispa/BatchSystem",
            description: "Output folder for the Condor Batch Manager"
          }
        }
      }
  });
  return JobSubmissionView;
});
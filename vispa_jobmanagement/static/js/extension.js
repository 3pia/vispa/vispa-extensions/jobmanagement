define(["vispa/extension",
  "./jobdashboard/dashboard",
  "./jobsubmission/submission",
  "./jobdesigner/designer"],
  function(Extension,JobDashboard,JobSubmission,JobDesigner) {
    var JobManagementExtension = Extension._extend({

        init: function init() {
          var self = this;
          init._super.call(this,"jobmanagement","JobManagement");
          this.mainMenuAdd([
            this.addView(JobSubmission),
            this.addView(JobDashboard),
            this.addView(JobDesigner)
          ]);
        }
    });
  /*
    var JobSubmissionView = MainView._extend({
        init: function init(args) {
            var self = this;
            init._super.apply(this,arguments);
            this.addMenuEntry("change output folder", {
                label: "change output folder",
                iconClass: "glyphicon glyphicon-folder-close",
                buttonClass: "btn-default",
                callback: function () {
                    self.jobsubmission.showOutputPathDialog();
                }
            });
            this.jobsubmission = new JobSubmission(this);
            this.setLabel("Job Submission");
            this.setupState({
                commands: undefined
            },args);
        },

        render: function (node) {
            this.jobsubmission.renderContent(node);
        }
    },{
      iconClass: "glyphicon glyphicon-share-alt",
      label: "Job Submission",
      menuPosition: 51
    });
    */
/*
    var JobDashboardView = MainView._extend({
        init: function init(args) {
            var self = this;
            init._super.apply(this,arguments);
            this.jobdashboard = new JobDashboard(this);
        },
        render: function (node) {
            this.jobdashboard.renderContent(node);
        }
    },{
      iconClass: "glyphicon glyphicon-list",
      label: "Job Dashboard",
      name: "JobDashboard",
      menuPosition: 50,
      menu: {
        refresh: {
          label: "refresh",
          iconClass: "glyphicon glyphicon-refresh",
          callback: function () {
            this.jobdashboard.refreshJobList();
          }
        },
        restartJobs: {
          label: "restart selected jobs",
          iconClass: "glyphicon glyphicon-retweet",
          callback: function(){
            this.jobdashboard.restartSelectedJobs(this.jobdashboard.getSelectedJobs());
          }
        },
        stopJobs: {
          label: "stop selected jobs",
          iconClass: "glyphicon glyphicon-stop",
          callback: function(){
            this.jobdashboard.stopSelectedJobs(this.jobdashboard.getSelectedJobs());
          }
        },
        removeJobs: {
          label: "remove selected jobs",
          iconClass: "glyphicon glyphicon-trash",
          callback: function(){
            this.jobdashboard.removeSelectedJobs(this.jobdashboard.getSelectedJobs());
          }
        },
        changeOutputFolder: {
          label: "change output folder",
          iconClass: "glyphicon glyphicon-folder-close",
          callback: function(){
            this.jobdashboard.showOutputPathDialog();
          }
        }
      },
      preferences: {
        items:{
          localOutputPath: {
            label: "localOutputFolder",
            level: 1,
            type: "string",
            value: "$HOME/.vispa/BatchSystem",
            description: "Output folder for the Local Batch Manager"
          },
          condorOutputPath: {
            label: "condorOutputFolder",
            level: 1,
            type: "string",
            value: "$HOME/.vispa/BatchSystem",
            description: "Output folder for the Condor Batch Manager"
          }
        }
      }
  });
  */
  /*
    var JobDesignerView = MainView._extend({
        init: function init(args){
            init._super.apply(this,arguments);
            this.jobdesigner = new JobDesigner(this);
            this.setupState({
                options: undefined
            },args);
        },
        render: function(node){
            this.jobdesigner.renderContent(node);
        }
    },{
      iconClass: "glyphicon glyphicon-wrench",
      label: "Job Designer",
      name: "JobDesigner",
      menuPosition: 52
    });
    */
    return JobManagementExtension;
});